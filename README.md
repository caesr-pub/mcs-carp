# MCS-PMSI: Designing Predictable Data Sharing Protocols for Multi-Core Mixed-Criticality Systems

This is the gem5 code repository for Predictable MCS-PMSI presented in our RTSS'18 paper.

# Getting started
* The Gem5 simulator has been modified to support predictable snoopy bus architecture, and trace based simulation. 
* `$GEM5` refers to the top level directory where gem5 resides. 
* Files `$GEM5/build_opts/X86_MCS_PMSI` and `$GEM5/build_opts/X86_MCS_PMSI_C2C` contains information for building the PMSI system, and `$GEM5/configs/ruby/MCS_PMSI.py` `$GEM5/configs/ruby/MCS_PMSI_C2C.py` provides information about the component (caches, network, DRAM) connections in the system. Note that these files do not need to be changed unless the cache hierarchy is extended. 

# Bus arbitration
* We extend Gem5 to support a snoopy bus with a configurable bus arbitration. 
* The `NPROC` and `SLOT_WIDTH` parameters in `src/cpu/testers/rubyTest/Trace.hh` control the number of requestors and the TDM slot width per requestor.

# Memory trace simulation
* Gem5 has been extended to run memory trace based execution.
* The trace based simulation injects traces into the ruby memory model thereby bypassing the core/processor. 
* Trace based simulation can be enabled by setting the `TRACE` flag in `src/cpu/testers/rubytest/Trace.hh`.	
* The trace based simulation reads from files `trace0.trc, trace1.trc, trace2,trc,..trace[NPROC-1].trc` that consists of lines of requests of the form `Addr OP time`. OP is of type RD for read and WR for write, and time is a positive integer value that denotes the arrival time of the memory request to the memory hierarchy.

# Building gem5 with MCS-PMSI protocol
## Configuration before building MCS-PMSI 
* The configuration options are provided in src/cpu/testers/rubytest/Trace.hh 
* `NPROC` - Total number of cores.
* `TRACE` - If set to 1: trace-based simulations, if set to 0: normal binary execution.
* `SLOT_WIDTH` - TDM slot width set in terms of cycles.
* `HRT_CORES` - list of HRT cores IDs.
* `HRT_CORES` - list of HRT cores IDs.
* `HRT_CORES` - list of HRT cores IDs.
* `HRT_SLOT_ALLOCATION` - Defines the slot allocation of the HRT cores. E.g, if {0,0,1,0,1} defined, then arbiter allocates dedicated HRT slots in the following order: core0, core0, core1, core0, core1.
* `TOTAL_DEDICATED_SRT_SLOTS` - Total dedicated number of SRT slots plus a reserve slot per TDM.
* `C2C_TYPE` if set to 1: NO cache to cache; if set to 2: cache to cache.

When assigning core types, TOTAL cores `NPROC` must be equal to |HRT| + |SRT| + |NRT|. A TDM period is the sum of HRT cores and the total number of dedicated SRT SLOTS.

## Additional configuration
* If trace based simulation is used set the latency in `$gem5/src/mem/SimpleMemory.py` tp `22ns`, and for binary execution set it to `9.5ns`

## Building
* To build MCS-PMSI with out cache to cache, execute `scons $GEM5/build/MCS_PMSI/gem5.opt -j8`.
* To build MCS-PMSI with cache to cache, execute `scons $GEM5/build/MCS_PMSI_C2C/gem5.opt -j8`.

# Running MCS-PMSI
* To run MCS-PMSI, use the following option: `--ruby, --cpu-type=SimpleMemory, --cpu-clock=xGHz --ruby-clock=yGHz`.
* Other options such as cache sizes, memory size, cache associativity, and number of cores can be configured accordingly.
* For binary/normal execution, supply the binary name and the associated arguments using the `-c` and `-o` flags respectively. For example, to run FFT of the Splash2 benchmark on MCS_PMSI with out cache to cache: `./build/X86_MCS_PMSI/gem5.opt --stats-file=FFT-pmsi-stats.out ./configs/example/se.py --ruby -n8 --cpu-type=timing --l1d_size=16kB --l1i_size=16kB --l1d_assoc=1 --l1i_assoc=1  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c $SPLASH/splash2/codes/kernels/fft/fft -o "-m 10 -p 8 -t"`
* For trace based execution, populate the `trace0.trc, trace1.trc, trace2,trc,...trace[NPROC-1].trc` file with the necessary memory operations and execute: `./build/X86_MCS_PMSI/gem5.opt ./configs/example/ruby_random_test.py --ruby-clock=2GHz --ruby --cpu-clock=2GHz --topology=Crossbar --mem-type=SimpleMemory -n 8 --mem-size=4194304kB --wakeup_freq=1`. 
* Note that the trace files should be in $GEM5, otherwise you can provide the path by using the option `--trace_path`
* We provide a couple of scripts to run the SPLASH2 benchmarks(`splash2-launch.sh`) and EEMBC benchmarks (`eembc-launch.sh`) including the traces and binaires.

# Contact
* Feel free to contact [us](mailto:amkaushi@uwaterloo.ca) for questions regarding MCS-PMSI.
