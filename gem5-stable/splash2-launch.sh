#CHOLESKY
echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=ocean-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4 --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/cholesky/cholesky -o \"-p8 splash2/codes/kernels/cholesky/inputs/tk14.O\" 

./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=cholesky-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4 --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/cholesky/cholesky -o "-p8 splash2/codes/kernels/cholesky/inputs/tk14.O" &> m5out/cholesky-predMSI-c2c.debug  & 
#mv trace-file.out cholesky-trace-file.out
#grep -Eo "MIC\|.*" buff > CHOLESKY-intercore-write-hits-wc-final.csv
#grep -Eo "MW\|.*" buff > CHOLESKY-write-hits-wc-final.csv
#grep -Eo "MAL\|.*" buff > CHOLESKY-arbit-write-hits-wc-final.csv
#grep -Eo "MICA\|.*" buff > CHOLESKY-intracore-write-hits-wc-final.csv

#FFT
echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt   --stats-file=FFT-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/fft/fft -o \"-m 10 -p 8 -t\"
./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt   --stats-file=FFT-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/fft/fft -o "-m 10 -p 8 -t" &> m5out/FFT-predMSI-c2c.debug &

#mv trace-file.out fft-trace-file.out
#grep -Eo "MIC\|.*" buff > FFT-intercore-write-hits-wc-final.csv
#grep -Eo "MW\|.*" buff > FFT-write-hits-wc-final.csv
#grep -Eo "MAL\|.*" buff > FFT-arbit-write-hits-wc-final.csv
#grep -Eo "MICA\|.*" buff > FFT-intracore-write-hits-wc-final.csv


# # FMM
 echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=FMM-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/fmm/FMM 
 ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=FMM-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/fmm/FMM &> m5out/FMM-predMSI-c2c.debug &

# #grep -Eo "MIC\|.*" buff > FMM-intercore-write-hits-wc-final.csv
# #grep -Eo "MW\|.*" buff > FMM-write-hits-wc-final.csv
# #grep -Eo "MAL\|.*" buff > FMM-arbit-write-hits-wc-final.csv
# #grep -Eo "MICA\|.*" buff > FMM-intracore-write-hits-wc-final.csv

# #LUCont
 echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=LU-cont-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing  --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4 --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/lu/contiguous_blocks/lu -o \"-n128 -p8\"
 ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=LU-cont-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing  --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4 --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/lu/contiguous_blocks/lu -o "-n128 -p8" &> m5out/LU-cont-predMSI-c2c.debug &

# #grep -Eo "MIC\|.*" buff > LU-intercore-write-hits-wc-final.csv
# #grep -Eo "MW\|.*" buff > LU-write-hits-wc-final.csv
# #grep -Eo "MAL\|.*" buff > LU-arbit-write-hits-wc-final.csv
# #grep -Eo "MICA\|.*" buff > LU-intracore-write-hits-wc-final.csv


# #mv trace-file.out lu-cont-trace-file.out 

# # OCEAN-cont
 echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=ocean-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/ocean/contiguous_partitions/OCEAN -o \"-n18 -p8\"
 ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=ocean-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/ocean/contiguous_partitions/OCEAN -o "-n18 -p8" &> m5out/ocean-predMSI-c2c.debug &

# #grep -Eo "MIC\|.*" buff > OCEAN-intercore-write-hits-wc-final.csv
# #grep -Eo "MW\|.*" buff > OCEAN-write-hits-wc-final.csv
# #grep -Eo "MAL\|.*" buff > OCEAN-arbit-write-hits-wc-final.csv
# #grep -Eo "MICA\|.*" buff > OCEAN-intracore-write-hits-wc-final.csv


# #mv trace-file.out ocean-cont-trace-file.out

# # RADIOSITY
 echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=Radiosity-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/radiosity/RADIOSITY -o \"-p 8 -batch\"
 ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt --stats-file=Radiosity-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/radiosity/RADIOSITY -o "-p 8 -batch" &> m5out/Radiosity-predMSI-c2c.debug & 

# #grep -Eo "MIC\|.*" buff > RADIOSITY-intercore-write-hits-wc-final.csv
# #grep -Eo "MW\|.*" buff > RADIOSITY-write-hits-wc-final.csv
# #grep -Eo "MAL\|.*" buff > RADIOSITY-arbit-write-hits-wc-final.csv
# #grep -Eo "MICA\|.*" buff > RADIOSITY-intracore-write-hits-wc-final.csv


# #mv trace-file.out radiosity-trace-file.out

# #RADIX
 echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=Radix-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/radix/RADIX -o \"-p8 -n16384\"
 ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=Radix-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/radix/RADIX -o "-p8 -n16384" &> m5out/Radix-predMSI-c2c.debug &

# #grep -Eo "MIC\|.*" buff > RADIX-intercore-write-hits-wc-final.csv
# #grep -Eo "MW\|.*" buff > RADIX-write-hits-wc-final.csv
# #grep -Eo "MAL\|.*" buff > RADIX-arbit-write-hits-wc-final.csv
# #grep -Eo "MICA\|.*" buff > RADIX-intracore-write-hits-wc-final.csv

# # RAYTRACE
 echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=raytrace-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/raytrace/RAYTRACE -o \"-p8 splash2/codes/apps/raytrace/teapot.env\"
 ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt --stats-file=PROPOSED-Raytrace-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/raytrace/RAYTRACE -o "-p8 splash2/codes/apps/raytrace/teapot.env" &> m5out/raytrace-predMSI-c2c.debug &

# #mv trace-file.out raytrace-trace-file.out
# #grep -Eo "MIC\|.*" buff > RAYTRACE-intercore-write-hits-wc-final.csv
# #grep -Eo "MW\|.*" buff > RAYTRACE-write-hits-wc-final.csv
# #grep -Eo "MAL\|.*" buff > RAYTRACE-arbit-write-hits-wc-final.csv
# #grep -Eo "MICA\|.*" buff > RAYTRACE-intracore-write-hits-wc-final.csv

# #BARNES
  echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=barnes-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/barnes/BARNES
 ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=barnes-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/barnes/BARNES &> m5out/barnes-predMSI-c2c.debug &

# #grep -Eo "MIC\|.*" buff > BARNES-intercore-write-hits-wc-final.csv
# #grep -Eo "MW\|.*" buff > BARNES-write-hits-wc-final.csv
# #grep -Eo "MAL\|.*" buff > BARNES-arbit-write-hits-wc-final.csv
# #grep -Eo "MICA\|.*" buff > BARNES-intracore-write-hits-wc-final.csv

# #mv trace-file.out barnes-trace-file.out

# # COULD HAVE PROBLEMS WITH THESE BENCHMARKS 

# #OCEAN nonCont
#echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=ocean-noncont-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/ocean/non_contiguous_partitions/OCEAN -o \"-p8 -n18\"
# #./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=ocean-noncont-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/apps/ocean/non_contiguous_partitions/OCEAN -o "-p8 -n18"
# #mv trace-file.out ocean-non-cont-trace-file.out

# #LUNonCont
# #echo ./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=LU-noncont-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/lu/non_contiguous_blocks/LU -o \"-n128 -p8\"
# #./build/X86_PMSI_1Level_Snooping_C2C/gem5.opt  --stats-file=LU-noncont-predMSI-c2c.out ./configs/example/se.py  --ruby -n8 --cpu-type=timing --l1d_size=32kB --l1i_size=32kB --l1d_assoc=4 --l1i_assoc=4  --mem-size=4194304kB --topology=Crossbar --cpu-clock=2GHz --ruby-clock=2GHz --mem-type=SimpleMemory -c splash2/codes/kernels/lu/non_contiguous_blocks/LU -o "-n128 -p8"
# #mv trace-file.out lu-non-cont-trace-file.out

# #WATER-NSQUARED
# #WATER-SPATIAL
# # FFT - different versions
# # RAYTRACE - different versions
