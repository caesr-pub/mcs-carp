#ifndef __TRACE_HH__
#define __TRACE_HH__

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

#define TRACE 1 // TRACE = 1 reads from trace.trc file. If not there, seg fault may happen, TRACE = 0 uses normal ruby tester when using ruby tester config file

#define WB_RANDOM 0
#define READ_WB_RANDOM 0
#define RANDOM_RECV 0
#define WRITE_HITS 0

#define PRED_ARB 1
#define DEADLOCK_CHECK 1
// BASELINE 1 does not use proposed solution
// BASELINE 0 uses proposed solution
//#define NPROC 4
#define BASELINE 0

#define SLOT_WIDTH 50
#define MAX_NPROC 8

// Enables slack slots
#define SLACK_SLOT

// Enables data dependency based slack slot allocation
// If commented set, use the DEFAULT-SSA
//#define DSA_SSA 1

// C2C_TYPE
// 1 No cache to cache
// 2 With cache to cache
#define C2C_TYPE 1

// WRITE_HITS 1, PRED_ARB 1, BASELINE 0 : PMSI transforms into conventional MSI on predictable arbiter, with upgrades that can happen any time.
// Make sure to use M(E)SI_Snooping_One_Level_RT
// WRITE_HITS X, PRED_ARB 0, BASELINE X : PMSI transforms into conventional MSI on conventional arbiter, with upgrades that can happen any time. 
// Make sure to use M(E)SI_Snooping_One_Level_RT_RRecv


////////////////////////////////////////////////////////////////////////////////
// When assigning the cores, |TOTAL cores| must be equal to
// |HRT| + |SRT| + |NRT|. A TDM period is the sum of HRT cores and the total
// number of dedicated SRT SLOTS. Total SRT dedicated slots must be less than
// total number of SRT cores
// +----------------------+------------------+
// |       HRT Slots      | SRT slots        |
// +----------------------+------------------+
// +--------------------TDM------------------+

const unsigned int retryThresh = 0;

// The set of HRT processors
const unsigned int HRT_CORES[] = {0,1,2,3};

// The set of SRT processors
const unsigned int SRT_CORES[] = {4,5,6};

// The set of NRT processors
const unsigned int NRT_CORES[] = {7};

const unsigned int CORE_0_LINKS[] = {0, 1, 2, 3, 4, 5, 6, 7};
const unsigned int CORE_1_LINKS[] = {0, 1, 2, 3, 4, 5, 6, 7};
const unsigned int CORE_2_LINKS[] = {0, 1, 2, 3, 4, 5, 6, 7};
const unsigned int CORE_3_LINKS[] = {0, 1, 2, 3, 4, 5, 6, 7};
const unsigned int CORE_4_LINKS[] = {4, 5, 6, 7};
const unsigned int CORE_5_LINKS[] = {4, 5, 6, 7};
const unsigned int CORE_6_LINKS[] = {4, 5, 6, 7};
const unsigned int CORE_7_LINKS[] = {7};

// Defines the slot allocation of HRT cores. E.g, if {0,0,1,0,1} defined, then
// arbiter allocates dedicated HRT slots in the following order:
// core0, core0, core1, core0, core1
const unsigned int HRT_SLOT_ALLOCATION[] = {0,0,0,0,1,1,1,1,2,2,3,3};

const unsigned int SUBSET_OF_CORE[] = {0,2,3,4,6};

//Total dedicated number of SRT slots plus a reserve slot per TDM 
const unsigned int TOTAL_DEDICATED_SRT_SLOTS = 3;

//helper macro to get total number of cores
inline  int getNumberOfCores(){
  return  sizeof(HRT_CORES)/sizeof(int) +
   sizeof(SRT_CORES)/sizeof(int)+
   sizeof(NRT_CORES)/sizeof(int);
}

//helper macro to get total number of HRT cores
inline int getNumberOfHRTCores(){
  return sizeof(HRT_CORES)/sizeof(unsigned int);
}

inline int getCore0Links() {
	return sizeof(CORE_0_LINKS)/sizeof(unsigned int);
}

inline int getCore1Links() {
	return sizeof(CORE_1_LINKS)/sizeof(unsigned int);
}

inline int getCore2Links() {
	return sizeof(CORE_2_LINKS)/sizeof(unsigned int);
}

inline int getCore3Links() {
	return sizeof(CORE_3_LINKS)/sizeof(unsigned int);
}

inline int getCore4Links() {
	return sizeof(CORE_4_LINKS)/sizeof(unsigned int);
}

inline int getCore5Links() {
	return sizeof(CORE_5_LINKS)/sizeof(unsigned int);
}

inline int getCore6Links() {
	return sizeof(CORE_6_LINKS)/sizeof(unsigned int);
}

inline int getCore7Links() {
	return sizeof(CORE_7_LINKS)/sizeof(unsigned int);
}
//helper macro to get total number of SRT cores
inline int getNumberOfSRTCores() {
  return sizeof(SRT_CORES)/sizeof(unsigned int);
}

//helper macro to get total number of NRT cores
inline int getNumberOfNRTCores(){
  return sizeof(NRT_CORES)/sizeof(unsigned int);
}

//helper macro to get total number of dedicated slots
inline int getTotalDedicatedSlots(){
  return sizeof(HRT_SLOT_ALLOCATION)/sizeof(unsigned int) + TOTAL_DEDICATED_SRT_SLOTS;
}

//helper macro to get total number of dedicated slots
inline int getTotalDedicatedHRTSlots(){
  return sizeof(HRT_SLOT_ALLOCATION)/sizeof(unsigned int);
}

inline bool isHRTCore(unsigned int core){
  for(int i=0; i < getNumberOfHRTCores(); i++)
    if(HRT_CORES[i] == core) return true;
  return false;
}

inline bool isSRTCore(unsigned int core){
  for(int i=0; i < getNumberOfSRTCores(); i++)
    if(SRT_CORES[i] == core) return true;
  return false;
}

inline bool isCountLessThresh(int retryCount) {
	if (retryCount < retryThresh) {
		return true;
	}
	return false;
}

inline bool isNRTCore(unsigned int core){
  for(int i=0; i < getNumberOfNRTCores(); i++)
    if(NRT_CORES[i] == core) return true;
  return false;
}

inline int getHRTCoreOfIndex(int indexHRT){
  if(getNumberOfHRTCores() > indexHRT)
    return HRT_CORES[indexHRT];
  else return -1;
}

inline int getSRTCoreOfIndex(int indexSRT){
  if(getNumberOfSRTCores() > indexSRT)
    return SRT_CORES[indexSRT];
  else return -1;
}

inline int getNRTCoreOfIndex(int indexNRT){
  if(getNumberOfNRTCores() > indexNRT)
    return NRT_CORES[indexNRT];
  else return -1;
}

inline int getIndexOfSRTCore(int core){
  for(int index = 0;index< getNumberOfSRTCores(); index++){
    if(SRT_CORES[index] == core)
      return index;
  }
  return -1;
}

inline bool
checkConnection(unsigned int a, unsigned int b) {
	// a: receiver
	// b: requestor
	if (a == 0) {
		for (unsigned int i = 0; i<getCore0Links(); i++) {
			if (b == CORE_0_LINKS[i]) return true;	
		}
		return false;
	}
	else if (a == 1) {
		for (unsigned int i = 0; i<getCore1Links(); i++) {
			if (b == CORE_1_LINKS[i]) return true;	
		}
		return false;
	}
	else if (a == 2) {
		for (unsigned int i = 0; i<getCore2Links(); i++) {
			if (b == CORE_2_LINKS[i]) return true;	
		}
		return false;
	}
	else if (a == 3) {
		for (unsigned int i = 0; i<getCore3Links(); i++) {
			if (b == CORE_3_LINKS[i]) return true;	
		}
		return false;
	}
	else if (a == 4) {
		for (unsigned int i = 0; i<getCore4Links(); i++) {
			if (b == CORE_4_LINKS[i]) return true;	
		}
		return false;
	}
	else if (a == 5) {
		for (unsigned int i = 0; i<getCore5Links(); i++) {
			if (b == CORE_5_LINKS[i]) return true;	
		}
		return false;
	}
	else if (a == 6) {
		for (unsigned int i = 0; i<getCore6Links(); i++) {
			if (b == CORE_6_LINKS[i]) return true;	
		}
		return false;
	}
	else {
		for (unsigned int i = 0; i<getCore7Links(); i++) {
			if (b == CORE_7_LINKS[i]) return true;	
		}
		return false;
	}
	return false;
}

#endif
