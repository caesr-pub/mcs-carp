/*
 * Copyright (c) 1999-2013 Mark D. Hill and David A. Wood
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

machine(Directory, "MSI Two Level directory protocol")
 : DirectoryMemory * directory;
   Cycles to_mem_ctrl_latency := 5;
   Cycles directory_latency := 6;

   MessageBuffer * requestToDir, network="From", virtual_network="2",
        ordered="false", vnet_type="request";
	 MessageBuffer * requestToDir_WB, network="From", virtual_network="6",
	 			ordered="false", vnet_type="request";

   MessageBuffer * responseToDir, network="From", virtual_network="4",
        ordered="false", vnet_type="response";
   MessageBuffer * responseFromDir, network="To", virtual_network="4",
        ordered="false", vnet_type="response";
{
  // STATES
  state_declaration(State, desc="Directory states", default="Directory_State_I") {
    // Base states
    I, AccessPermission:Read_Write, desc="lsndf";
    II, AccessPermission:Read_Write, desc="lsndf";
    II_D, AccessPermission:Read_Write, desc="lsndf";
		S, AccessPermission:Read_Write, desc="lknsdf";
		EoM, AccessPermission:Read_Write, desc="lsjndf";
		O, AccessPermission:Read_Write,desc="kjsbdf";
		IS, AccessPermission:Read_Write,desc="kjsbdf";
		IS_D, AccessPermission:Read_Write,desc="kjsbdf";
		IM_D, AccessPermission:Read_Write,desc="kjsbdf";
		IM, AccessPermission:Read_Write,desc="kjsbdf";
		SS, AccessPermission:Read_Write,desc="kjsbdf";
		EoM_D, AccessPermission:Read_Write,desc="kjsbdf";
		EoM_DD, AccessPermission:Read_Write,desc="kjsbdf";
		EoM_SD, AccessPermission:Read_Write,desc="kjsbdf";
		O_D, AccessPermission:Read_Write,desc="kjsbdf";
  }

  // Events
  enumeration(Event, desc="Directory events") {
    Fetch, desc="A memory fetch arrives";
    Data, desc="writeback data arrives";
    Data_Empty, desc="writeback data arrives";
    Memory_Data, desc="Fetched data from memory arrives";
    Memory_Ack, desc="Writeback Ack from memory arrives";

    DMA_READ, desc="A DMA Read memory request";
    DMA_WRITE, desc="A DMA Write memory request";

		GETS, desc="A GETS request";
    PUTM, desc="A putm arrives";
		GETM, desc="A GETM request";
		GETMO, desc="A GETM request";
		UPG, desc="A UPG request";
		GETMR, desc="A GETM request";

		UPG_NP, desc="A UPG request";
		GETM_NP, desc="A GETM request";
		GETS_NP, desc="A GETS request";
  }

  // TYPES

  // DirectoryEntry
  structure(Entry, desc="...", interface="AbstractEntry") {
    State DirectoryState,          desc="Directory state";
    NetDest Owner, desc="Owner of this block";
		NetDest Sharers, desc="Sharers of this block";
  }

  // TBE entries for DMA requests
  structure(TBE, desc="TBE entries for outstanding DMA requests") {
    Address PhysicalAddress, desc="physical address";
    State TBEState,        desc="Transient State";
    DataBlock DataBlk,     desc="Data to be written (DMA write only)";
    int Len,               desc="...";

		NetDest L1_GetS_IDs, desc="Set of the internal processors that want this in shared state";
		NetDest L1_Get_IDs, desc="Set of the internal processors that want this";
		MachineID L1_GetM_ID, desc="Processor that wants this in modified state";
  }

  structure(TBETable, external="yes") {
    TBE lookup(Address);
    void allocate(Address);
    void deallocate(Address);
    bool isPresent(Address);
    bool functionalRead(Packet *pkt);
    int functionalWrite(Packet *pkt);
  }


  // ** OBJECTS **
  TBETable TBEs, template="<Directory_TBE>", constructor="m_number_of_TBEs";

  void set_tbe(TBE tbe);
  void unset_tbe();
  void wakeUpBuffers(Address a);

  Entry getDirectoryEntry(Address addr), return_by_pointer="yes" {
    Entry dir_entry := static_cast(Entry, "pointer", directory[addr]);

    if (is_valid(dir_entry)) {
      return dir_entry;
    }

    dir_entry :=  static_cast(Entry, "pointer",
                              directory.allocate(addr, new Entry));
    return dir_entry;
  }

  State getState(TBE tbe, Address addr) {
    if (is_valid(tbe)) {
      return tbe.TBEState;
    } else if (directory.isPresent(addr)) {
      return getDirectoryEntry(addr).DirectoryState;
    } else {
      return State:I;
    }
  }

  void setState(TBE tbe, Address addr, State state) {
    if (is_valid(tbe)) {
      tbe.TBEState := state;
    }

    if (directory.isPresent(addr)) {
      getDirectoryEntry(addr).DirectoryState := state;
    }
  }

  AccessPermission getAccessPermission(Address addr) {
    TBE tbe := TBEs[addr];
    if(is_valid(tbe)) {
      DPRINTF(RubySlicc, "%s\n", Directory_State_to_permission(tbe.TBEState));
      return Directory_State_to_permission(tbe.TBEState);
    }

    if(directory.isPresent(addr)) {
      DPRINTF(RubySlicc, "%s\n", Directory_State_to_permission(getDirectoryEntry(addr).DirectoryState));
      return Directory_State_to_permission(getDirectoryEntry(addr).DirectoryState);
    }

    DPRINTF(RubySlicc, "%s\n", AccessPermission:NotPresent);
    return AccessPermission:NotPresent;
  }

  void functionalRead(Address addr, Packet *pkt) {
    TBE tbe := TBEs[addr];
    if(is_valid(tbe)) {
      testAndRead(addr, tbe.DataBlk, pkt);
    } else {
      functionalMemoryRead(pkt);
    }
  }

  int functionalWrite(Address addr, Packet *pkt) {
    int num_functional_writes := 0;

    TBE tbe := TBEs[addr];
    if(is_valid(tbe)) {
      num_functional_writes := num_functional_writes +
        testAndWrite(addr, tbe.DataBlk, pkt);
    }

    num_functional_writes := num_functional_writes + functionalMemoryWrite(pkt);
    return num_functional_writes;
  }

  void setAccessPermission(Address addr, State state) {
    if (directory.isPresent(addr)) {
      getDirectoryEntry(addr).changePermission(Directory_State_to_permission(state));
    }
  }



  // ** OUT_PORTS **
  out_port(responseNetwork_out, ResponseMsg, responseFromDir);

  // ** IN_PORTS **

	in_port(requestNetwork_in, RequestMsg, requestToDir, rank=0) {
		if (requestNetwork_in.isReady()) {
			peek(requestNetwork_in, RequestMsg) {
				assert(in_msg.Destination.isElement(machineID));
				if (in_msg.Type == CoherenceRequestType:GETS || in_msg.Type == CoherenceRequestType:GETI) {

					DPRINTF(RubySlicc, "GETS FROM: %s, OWNER: %s\n", in_msg.Requestor, getDirectoryEntry(in_msg.Addr).Owner);
					if (getDirectoryEntry(in_msg.Addr).Owner.isEmpty()) {	
						trigger(Event:GETS, in_msg.Addr, TBEs[in_msg.Addr]);
					}
					else {
						if (isDirectConnectDir(getDirectoryEntry(in_msg.Addr).Owner, in_msg.Requestor)) {
							trigger(Event:GETS, in_msg.Addr, TBEs[in_msg.Addr]);
						}
						else {
							trigger(Event:GETS_NP, in_msg.Addr, TBEs[in_msg.Addr]);	
						}
					}
        	//trigger(Event:GETS, in_msg.Addr, TBEs[in_msg.Addr]);
				}
        else if (in_msg.Type == CoherenceRequestType:GETM || in_msg.Type == CoherenceRequestType:GETMR || in_msg.Type == CoherenceRequestType:GETMO){
          DPRINTF(RubySlicc, "GETM FOR: %s FROM: %s\n", in_msg.Addr, in_msg.Requestor);
          //trigger(Event:GETM, in_msg.Addr, TBEs[in_msg.Addr]);

					if (getDirectoryEntry(in_msg.Addr).Owner.isEmpty()) {	
						trigger(Event:GETM, in_msg.Addr, TBEs[in_msg.Addr]);
					}
					else {
						if (isDirectConnectDir(getDirectoryEntry(in_msg.Addr).Owner, in_msg.Requestor)) {
							trigger(Event:GETM, in_msg.Addr, TBEs[in_msg.Addr]);
						}
						else {
							trigger(Event:GETM_NP, in_msg.Addr, TBEs[in_msg.Addr]);	
						}
					}
        }	
				else if (in_msg.Type == CoherenceRequestType:UPG) {
					//trigger(Event:UPG, in_msg.Addr, TBEs[in_msg.Addr]);

					if (getDirectoryEntry(in_msg.Addr).Owner.isEmpty()) {	
						trigger(Event:UPG, in_msg.Addr, TBEs[in_msg.Addr]);
					}
					else {
						if (isDirectConnectDir(getDirectoryEntry(in_msg.Addr).Owner, in_msg.Requestor)) {
							trigger(Event:UPG, in_msg.Addr, TBEs[in_msg.Addr]);
						}
						else {
							trigger(Event:UPG_NP, in_msg.Addr, TBEs[in_msg.Addr]);	
						}
					}
				}
				else if (in_msg.Type == CoherenceRequestType:PUTM) {
					if (getDirectoryEntry(in_msg.Addr).Owner.isElement(in_msg.Requestor)) {
						DPRINTF(RubySlicc, "OWNER DOING PUTM: %s\n", in_msg.Addr);
						trigger(Event:PUTM, in_msg.Addr, TBEs[in_msg.Addr]);
					}
					/*
					else {
						DPRINTF(RubySlicc, "NON-OWNER DOING PUTM: %s\n", in_msg.Addr, getDirectoryEntry(in_msg.Addr).Owner);
						requestNetwork_in.dequeue();
					}
					*/
				}
				else {
					DPRINTF(RubySLicc, "%s\n", in_msg);
					error("Invalid message");
				}
			}
		}
	}

	in_port(requestNetworkWB_in, RequestMsg, requestToDir_WB, rank=0) {
		if (requestNetworkWB_in.isReady()) {
			peek(requestNetworkWB_in, RequestMsg) {
				assert(in_msg.Destination.isElement(machineID));
				if (in_msg.Type == CoherenceRequestType:GETS || in_msg.Type == CoherenceRequestType:GETI) {
					trigger(Event:GETS, in_msg.Addr, TBEs[in_msg.Addr]);
				}
				else if (in_msg.Type == CoherenceRequestType:GETM) {
					trigger(Event:GETM, in_msg.Addr, TBEs[in_msg.Addr]);
				}
				else if (in_msg.Type == CoherenceRequestType:UPG) {
					trigger(Event:UPG, in_msg.Addr, TBEs[in_msg.Addr]);
				}
				else if (in_msg.Type == CoherenceRequestType:PUTM) {
					if (getDirectoryEntry(in_msg.Addr).Owner.isElement(in_msg.Requestor)) {
						DPRINTF(RubySlicc, "OWNER DOING PUTM: %s\n", in_msg.Addr);
						trigger(Event:PUTM, in_msg.Addr, TBEs[in_msg.Addr]);
					}
					else {
						DPRINTF(RubySlicc, "NON-OWNER DOING PUTM: %s %s\n", in_msg.Addr, getDirectoryEntry(in_msg.Addr).Owner);
						requestNetworkWB_in.dequeue();
					}
				}
				else {
					DPRINTF(RubySLicc, "%s\n", in_msg);
					error("Invalid message");
				}
			}
		}
	}

  in_port(responseNetwork_in, ResponseMsg, responseToDir, rank = 1) {
    if (responseNetwork_in.isReady()) {
      peek(responseNetwork_in, ResponseMsg) {
        assert(in_msg.Destination.isElement(machineID));
        if (in_msg.Type == CoherenceResponseType:DATA_TO_WB) {
          trigger(Event:Data, in_msg.Addr, TBEs[in_msg.Addr]);
        }
				else if (in_msg.Type == CoherenceResponseType:DATA_TO_WB_EMPTY) {
					trigger(Event:Data_Empty, in_msg.Addr, TBEs[in_msg.Addr]);
				}
				else {
          DPRINTF(RubySlicc, "%s\n", in_msg.Type);
          error("Invalid message");
        }
      }
    }
  }

  // off-chip memory request/response is done
  in_port(memQueue_in, MemoryMsg, responseFromMemory, rank = 2) {
    if (memQueue_in.isReady()) {
      peek(memQueue_in, MemoryMsg) {
        if (in_msg.Type == MemoryRequestType:MEMORY_READ) {
          trigger(Event:Memory_Data, in_msg.Addr, TBEs[in_msg.Addr]);
        } 
				
				else if (in_msg.Type == MemoryRequestType:MEMORY_WB) {
          trigger(Event:Memory_Ack, in_msg.Addr, TBEs[in_msg.Addr]);
        }
				
				else {
          DPRINTF(RubySlicc, "%s\n", in_msg.Type);
          error("Invalid message");
        }
      }
    }
  }

	

  // Actions

	action(c_clearOwner, "c", desc="clear owner") {
		getDirectoryEntry(address).Owner.clear();
	}

	action(c_clearSharers, "cs", desc="clear sharers") {
		getDirectoryEntry(address).Sharers.clear();
	}

	action(e_ownerIsRequestor, "e", desc="owner is requestor") {
		peek(requestNetwork_in, RequestMsg) {
				
			DPRINTF(RubySlicc, "OWNER IS REQUESTOR %s\n", in_msg.Requestor);
			getDirectoryEntry(address).Owner.clear();
			getDirectoryEntry(address).Owner.add(in_msg.Requestor);			
		}
	}

	action(e_sharerIsRequestor ,"es", desc="sharers") {
		peek(requestNetwork_in, RequestMsg) {
			getDirectoryEntry(address).Sharers.add(in_msg.Requestor);
		}
	}


  action(a_sendAck, "a", desc="Send ack to L2") {
    peek(memQueue_in, MemoryMsg) {
      enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
        out_msg.Addr := address;
        out_msg.Type := CoherenceResponseType:MEMORY_ACK;
        out_msg.Sender := machineID;
        out_msg.Destination.add(in_msg.Sender);
        out_msg.MessageSize := MessageSizeType:Response_Control;
      }
    }
  }
	
	action(d_sendDataToCores, "dsc", desc="Send data to whoever requested") {
		assert(tbe.L1_Get_IDs.count() > 0);	
		peek(memQueue_in, MemoryMsg) {
			enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
				out_msg.Addr := address;
				out_msg.Type := CoherenceResponseType:DATA;
				out_msg.Sender := machineID;
				out_msg.Destination := tbe.L1_Get_IDs;
				out_msg.DataBlk := in_msg.DataBlk;
				out_msg.Dirty := false;
				out_msg.MessageSize := MessageSizeType:Response_Data;
				
				DPRINTF(RubySlicc, "Send data to requestor: %s\n", out_msg.Destination);
			}
		}
	}
	
	action(d_sendDataToCoresRN, "dscRN", desc="Send data to whoever requested") {
		assert(tbe.L1_Get_IDs.count() > 0);
		peek(responseNetwork_in, ResponseMsg) {
			enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
				out_msg.Addr := address;
				out_msg.Type := CoherenceResponseType:DATA;
				out_msg.Sender := machineID;
				out_msg.Destination := tbe.L1_Get_IDs;
				out_msg.DataBlk := in_msg.DataBlk;
				out_msg.Dirty := false;
				out_msg.MessageSize := MessageSizeType:Response_Data;
				
				DPRINTF(RubySlicc, "Send data to requestor: %s\n", out_msg.Destination);
			}
		}
	}


	action(d_sendDataToSharersRN, "dsRN", desc="Send data to sharers from responseNetwork") {
		assert(is_valid(tbe));
		assert(tbe.L1_GetS_IDs.count() > 0);
		peek(responseNetwork_in, ResponseMsg) {
			enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
				out_msg.Addr := address;
				out_msg.Type := CoherenceResponseType:DATA;
				out_msg.Sender := machineID;
				out_msg.Destination := tbe.L1_GetS_IDs;
				out_msg.DataBlk := in_msg.DataBlk;
				out_msg.Dirty := false;
					out_msg.MessageSize := MessageSizeType:Response_Data;
				DPRINTF(RubySlicc, "Send data to requestor: %s\n", out_msg.Destination);
			}
		}
	}

	action(d_sendDataToRequestorRN, "dreqresp", desc="Send data to requestor from ResponseNetwork") {		
		peek(responseNetwork_in, ResponseMsg) {
			enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
				out_msg.Addr := address;
				out_msg.Type := CoherenceResponseType:DATA;
				out_msg.Sender := machineID;
				out_msg.Destination.add(tbe.L1_GetM_ID);
				out_msg.DataBlk := in_msg.DataBlk;
				out_msg.MessageSize := MessageSizeType:Response_Data;
				DPRINTF(RubySlicc, "Send data to requestor: %s\n", out_msg.Destination);
			}
		}
	}

	action(d_sendDataToSharers, "ds", desc="Send data to sharers") {
		assert(is_valid(tbe));
		assert(tbe.L1_GetS_IDs.count() > 0);
		peek(memQueue_in, MemoryMsg) {
			enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
				out_msg.Addr := address;
				out_msg.Type := CoherenceResponseType:DATA;
				out_msg.Sender := machineID;
				out_msg.Destination := tbe.L1_GetS_IDs;
				out_msg.DataBlk := in_msg.DataBlk;
				out_msg.Dirty := false;
					out_msg.MessageSize := MessageSizeType:Response_Data;
				DPRINTF(RubySlicc, "Send data to requestor: %s\n", out_msg.Destination);
			}
		}
	}

	action(d_sendDataToRequestor, "dreq", desc="Send data to requestor") {
		peek(memQueue_in, MemoryMsg) {
			enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
				out_msg.Addr := address;
				out_msg.Type := CoherenceResponseType:DATA;
				out_msg.Sender := machineID;
				out_msg.Destination.add(tbe.L1_GetM_ID);
				out_msg.DataBlk := in_msg.DataBlk;
				out_msg.MessageSize := MessageSizeType:Response_Data;
				DPRINTF(RubySlicc, "Send data to requestor: %s\n", out_msg.Destination);
			}
		}
	}

  action(d_sendData, "d", desc="Send data to requestor") {
    peek(memQueue_in, MemoryMsg) {
      enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
        out_msg.Addr := address;
        out_msg.Type := CoherenceResponseType:DATA;
        out_msg.Sender := machineID;
        out_msg.Destination.add(in_msg.OriginalRequestorMachId);
        out_msg.DataBlk := in_msg.DataBlk;
        out_msg.Dirty := false;
        out_msg.MessageSize := MessageSizeType:Response_Data;       
				out_msg.SendTime := memQueue_in.dequeue();
				DPRINTF(RubySlicc, "Send data to requestor: %s\n", out_msg.Destination);
			}
    }
  }

  action(d_sendDataExcl, "dexcl", desc="Send data to requestor") {
    peek(memQueue_in, MemoryMsg) {
      enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
        out_msg.Addr := address;
        out_msg.Type := CoherenceResponseType:DATAE;
        out_msg.Sender := machineID;
        out_msg.Destination.add(in_msg.OriginalRequestorMachId);
        out_msg.DataBlk := in_msg.DataBlk;
        out_msg.Dirty := false;
        out_msg.MessageSize := MessageSizeType:Response_Data;       
				out_msg.SendTime := memQueue_in.dequeue();
			}
    }
  }


  // Actions
  action(aa_sendAck, "aa", desc="Send ack to L2") {
    peek(memQueue_in, MemoryMsg) {
      enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
        out_msg.Addr := address;
        out_msg.Type := CoherenceResponseType:MEMORY_ACK;
        out_msg.Sender := machineID;
        out_msg.Destination.add(in_msg.OriginalRequestorMachId);
        out_msg.MessageSize := MessageSizeType:Response_Control;
      }
    }
  }

	action(i_allocateTBE, "allocTBE", desc="allocate TBE") {
		TBEs.allocate(address);
		set_tbe(TBEs[address]);
	}

	action(w_deallocateTBE, "wtbe", desc="deallocate TBE") {
		TBEs.deallocate(address);
		unset_tbe();
	}

  action(j_popIncomingRequestQueue, "j", desc="Pop incoming request queue") {
    requestNetwork_in.dequeue();
		DPRINTF(RubySlicc, "Popping request network\n");
  }

	action(j_popIncomingRequestWBQueue, "jwb", desc="Pop incoming WB request queue") {
		requestNetworkWB_in.dequeue();
		DPRINTF(RubySlicc, "Popping request WB network\n");
	}

  action(k_popIncomingResponseQueue, "k", desc="Pop incoming request queue") {
    responseNetwork_in.dequeue();
  }

  action(l_popMemQueue, "q", desc="Pop off-chip request queue") {
    memQueue_in.dequeue();
  }

  action(kd_wakeUpDependents, "kd", desc="wake-up dependents") {
		DPRINTF(RubySlicc, "WAKING UP DEPEDNENTS %s\n", address);
    wakeUpBuffers(address);
  }

  action(qf_queueMemoryFetchRequest, "qf", desc="Queue off-chip fetch request") {
    peek(requestNetwork_in, RequestMsg) {
      queueMemoryRead(in_msg.Requestor, address, to_mem_ctrl_latency);
    }
  }

	action(qf_queueDirectMemoryFetchRequest, "qfdirecto", desc="Queue off-chip fetch request") {
		queueMemoryRead(tbe.L1_GetM_ID, address, to_mem_ctrl_latency);
	}

  action(qw_queueMemoryWBRequest, "qw", desc="Queue off-chip writeback request") {
    peek(responseNetwork_in, ResponseMsg) {
      queueMemoryWrite(in_msg.Sender, address, to_mem_ctrl_latency,
                       in_msg.DataBlk);
    }
  }

  action(qf_queueMemoryFetchRequestDMA, "qfd", desc="Queue off-chip fetch request") {
    peek(requestNetwork_in, RequestMsg) {
      queueMemoryRead(in_msg.Requestor, address, to_mem_ctrl_latency);
    }
  }


  action(p_popIncomingDMARequestQueue, "p", desc="Pop incoming DMA queue") {
    requestNetwork_in.dequeue();
  }

  action(dr_sendDMAData, "dr", desc="Send Data to DMA controller from directory") {
    peek(memQueue_in, MemoryMsg) {
      enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
        out_msg.Addr := address;
        out_msg.Type := CoherenceResponseType:DATA;
        out_msg.DataBlk := in_msg.DataBlk;   // we send the entire data block and rely on the dma controller to split it up if need be
        out_msg.Destination.add(map_Address_to_DMA(address));
        out_msg.MessageSize := MessageSizeType:Response_Data;
      }
    }
  }

  action(qw_queueMemoryWBRequest_partial, "qwp",desc="Queue off-chip writeback request") {
    peek(requestNetwork_in, RequestMsg) {
      queueMemoryWritePartial(machineID, address, to_mem_ctrl_latency,
                              in_msg.DataBlk, in_msg.Len);
    }
  }

  action(da_sendDMAAck, "da", desc="Send Ack to DMA controller") {
      enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
        out_msg.Addr := address;
        out_msg.Type := CoherenceResponseType:ACK;
        out_msg.Destination.add(map_Address_to_DMA(address));
        out_msg.MessageSize := MessageSizeType:Writeback_Control;
      }
  }

	action(z_stall, "zs", desc="stall") {
		// Do nothin
	}

  action(z_stallAndWaitRequest, "z", desc="recycle request queue") {
		DPRINTF(RubySlicc, "STALLING\n");
    stall_and_wait(requestNetwork_in, address);
  }


  action(zz_recycleDMAQueue, "zz", desc="recycle DMA queue") {
    requestNetwork_in.recycle();
  }


	action(ss_recordGetSL1ID, "\s", desc="Record L1 gets") {
		peek(requestNetwork_in, RequestMsg) {
			tbe.L1_GetS_IDs.add(in_msg.Requestor);
			tbe.L1_Get_IDs.add(in_msg.Requestor);
		}
	}


  action(drp_sendDMAData, "drp", desc="Send Data to DMA controller from incoming PUTX") {
    peek(responseNetwork_in, ResponseMsg) {
      enqueue(responseNetwork_out, ResponseMsg, to_mem_ctrl_latency) {
        out_msg.Addr := address;
        out_msg.Type := CoherenceResponseType:DATA;
        out_msg.DataBlk := in_msg.DataBlk;   // we send the entire data block and rely on the dma controller to split it up if need be
        out_msg.Destination.add(map_Address_to_DMA(address));
        out_msg.MessageSize := MessageSizeType:Response_Data;
      }
    }
  }

  action(v_allocateTBE, "v", desc="Allocate TBE") {
    peek(requestNetwork_in, RequestMsg) {
      TBEs.allocate(address);
      set_tbe(TBEs[address]);
      tbe.DataBlk := in_msg.DataBlk;
      tbe.PhysicalAddress := in_msg.Addr;
      tbe.Len := in_msg.Len;
    }
  }


  action(qw_queueMemoryWBRequest_partialTBE, "qwt",desc="Queue off-chip writeback request") {
    peek(responseNetwork_in, ResponseMsg) {
      queueMemoryWritePartial(in_msg.Sender, tbe.PhysicalAddress,
                              to_mem_ctrl_latency, tbe.DataBlk, tbe.Len);
    }
  }

  //*****************************************************
  // TRANSITIONS FROM I
  //*****************************************************
	
  transition(I, {GETS}, IS) {
      i_allocateTBE;
			c_clearOwner;
      e_ownerIsRequestor;
      qf_queueMemoryFetchRequest;
      j_popIncomingRequestQueue;
  }

  transition(II, {GETS}, IS) {
      i_allocateTBE;
      qf_queueMemoryFetchRequest;
      j_popIncomingRequestQueue;
  }

  transition(II, {GETM,UPG}, IM) {
      i_allocateTBE;
      qf_queueMemoryFetchRequest;
      j_popIncomingRequestQueue;
  }

  transition(II_D, {GETS}, IS_D) {
      i_allocateTBE;
      qf_queueMemoryFetchRequest;
      j_popIncomingRequestQueue;
  }

  transition(II_D, {GETM,UPG}, IM_D) {
      i_allocateTBE;
      qf_queueMemoryFetchRequest;
      j_popIncomingRequestQueue;
  }

	// GETS_NP will only happen if a non-linked core does a WB, that core will move to E (no change in owner)
  transition(I, {GETS_NP}, IS) {
      i_allocateTBE;
      qf_queueMemoryFetchRequest;
      j_popIncomingRequestQueue;
  }


	transition(EoM, UPG, IM) {
		c_clearOwner;
		e_ownerIsRequestor;
		i_allocateTBE;
		qf_queueMemoryFetchRequest;
		j_popIncomingRequestQueue;
	}

	transition(EoM, {UPG_NP,GETS_NP,GETM_NP}, EoM_D) {
		c_clearOwner;
		e_ownerIsRequestor;
		i_allocateTBE;
  	z_stallAndWaitRequest;
	}

	transition(EoM_D, {GETS}) {
		j_popIncomingRequestQueue;
	}

	transition(EoM_D, {GETM,UPG}) {
		e_ownerIsRequestor;
		j_popIncomingRequestQueue;
	}

	transition(EoM, GETMR, IM) {
		i_allocateTBE;
		qf_queueMemoryFetchRequest;
		j_popIncomingRequestQueue;
	}

  transition({IS}, Memory_Data, EoM) {
      d_sendDataExcl;
      w_deallocateTBE;
      kd_wakeUpDependents;
  }

  transition(IM, Memory_Data, EoM) {
      d_sendData;
      kd_wakeUpDependents;
      w_deallocateTBE;
  }

  transition({IS_D}, Memory_Data, EoM_SD) {
      d_sendDataExcl;
      w_deallocateTBE;
      kd_wakeUpDependents;
			i_allocateTBE;
  }

  transition(IM_D, Memory_Data, EoM_SD) {
      d_sendData;
      kd_wakeUpDependents;
      w_deallocateTBE;
			i_allocateTBE;
  }

	transition(EoM_SD, GETM, EoM_D) {
  	z_stallAndWaitRequest;
	}

  transition(S, GETS, SS) {
      i_allocateTBE;
      ss_recordGetSL1ID;
      qf_queueMemoryFetchRequest;
      j_popIncomingRequestQueue;
  }

  transition(SS, Memory_Data, S) {
      d_sendData;
      w_deallocateTBE;
      kd_wakeUpDependents;
  }

  transition({IS,IM,SS,IS_D,IM_D}, {GETS,GETM,UPG,GETS_NP,GETM_NP,UPG_NP}) {
  	z_stallAndWaitRequest;
  }

  transition({S,I}, {GETM,GETM_NP,UPG_NP,UPG}, IM) {
      e_ownerIsRequestor;
      i_allocateTBE;
      qf_queueMemoryFetchRequest;
      j_popIncomingRequestQueue;
  }


  //*****************************************************
  // TRANSITIONS FROM EoM
  //*****************************************************
	
	transition(EoM, GETS, O) {
		j_popIncomingRequestQueue;	
	}

	transition(EoM, GETM) {
			e_ownerIsRequestor;
			j_popIncomingRequestQueue;
	}

	transition(EoM, PUTM, EoM_D) {
		  i_allocateTBE;
      j_popIncomingRequestWBQueue;
	}

	transition({EoM_D,O_D}, PUTM) {
			j_popIncomingRequestWBQueue;
	}

	transition(EoM_D, Data, II) {
    qw_queueMemoryWBRequest;
    k_popIncomingResponseQueue;
    w_deallocateTBE;
    kd_wakeUpDependents;
	}

	transition(EoM_DD, Data, II_D) {
		qw_queueMemoryWBRequest;
		k_popIncomingResponseQueue;
		w_deallocateTBE;
		kd_wakeUpDependents;
	}

	transition(EoM_D, Data_Empty, II) {
		c_clearOwner;
    k_popIncomingResponseQueue;
    w_deallocateTBE;
    kd_wakeUpDependents;
	}

  //*****************************************************
  // TRANSITIONS FROM O 
  //*****************************************************
	
	transition(O, PUTM, O_D) {
			i_allocateTBE;
			j_popIncomingRequestWBQueue;
	}

	transition({EoM_D, O_D}, {GETS_NP}, EoM_DD) {
		z_stallAndWaitRequest;
	}

	transition({EoM_D,O_D}, {GETM_NP,UPG_NP}, EoM_DD) {
		e_ownerIsRequestor;
		z_stallAndWaitRequest;
	}

	transition(O, {GETM,UPG},  EoM) {
		e_ownerIsRequestor;
		j_popIncomingRequestQueue;
	}

	transition(O, {GETM_NP,UPG_NP,GETS_NP}, O_D) {
			e_ownerIsRequestor;
			i_allocateTBE;
			z_stallAndWaitRequest;
	}

	transition(O_D, {GETM,GETS,UPG}) {
		j_popIncomingRequestQueue;
	}

	transition(O, GETS) {
		j_popIncomingRequestQueue;
	}
	
	// Owner did WB, that means only shared requestors can exist, and none of them in E or O state
	transition(O_D, Data, II) {
    qw_queueMemoryWBRequest;
    k_popIncomingResponseQueue;
    w_deallocateTBE;
    kd_wakeUpDependents;	
	}

	transition({II,S,EoM,O,IS,IM,SS,EoM_D,O_D,IS_D,IM_D,EoM_DD},Memory_Ack) {
		l_popMemQueue;
	}
}
