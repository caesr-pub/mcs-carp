/*
 * Copyright (c) 1999-2008 Mark D. Hill and David A. Wood
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <algorithm>

#include "base/cast.hh"
#include "base/random.hh"
#include "debug/RubyNetwork.hh"
#include "mem/ruby/network/simple/PerfectSwitch.hh"
#include "mem/ruby/network/MessageBuffer.hh"
#include "mem/ruby/network/simple/SimpleNetwork.hh"
#include "mem/ruby/network/simple/Switch.hh"
#include "mem/ruby/slicc_interface/NetworkMessage.hh"
#include "mem/ruby/system/System.hh"

#include "mem/ruby/network/Network.hh"

#include "sim/debug.hh"

#include <iostream>
#include <sstream>
#include <string>
#include <iterator>

#include "mem/protocol/RequestMsg.hh"
#include "mem/protocol/ResponseMsg.hh"

using namespace std;

std::map<Address, std::vector<int> >addressCoreRetryMap;
const int PRIORITY_SWITCH_LIMIT = 128;

// Operator for helper class
bool
operator<(const LinkOrder& l1, const LinkOrder& l2)
{
  return (l1.m_value < l2.m_value);
}

PerfectSwitch::PerfectSwitch(SwitchID sid, Switch *sw, uint32_t virt_nets)
  : Consumer(sw)
{
  m_switch_id = sid;
  m_round_robin_start = 0;
  m_wakeups_wo_switch = 0;
  m_virtual_networks = virt_nets;
}

void
PerfectSwitch::init(SimpleNetwork *network_ptr)
{
  m_network_ptr = network_ptr;

  for(int i = 0;i < m_virtual_networks;++i) {
    m_pending_message_count.push_back(0);
  }

  m_pending_message_count_mtx.resize(m_virtual_networks,
                                     vector<int>(m_in.size(), 0) );

  if(m_switch_id == 9){
    DPRINTF(RubyNetwork, "init: switch_id:%s schedule %s\n", m_switch_id,SLOT_WIDTH);
    scheduleEvent(Cycles(SLOT_WIDTH));

    m_is_slack_slot = false;
    m_srt_idx = 0;
    m_slot_owner = getHRTCoreOfIndex(getSlotOwnerNew());

    for(int i = 0; i< getNumberOfCores(); i++){
      m_token.push_back(false);
      DPRINTF(RubyNetwork, "init: m_token[%s]:%s\n", i, m_token[i]);
    }
  }
}

void
PerfectSwitch::addInPort(const vector<MessageBuffer*>& in)
{
  NodeID port = m_in.size();
  m_in.push_back(in);

  for (int i = 0; i < in.size(); ++i) {
    if (in[i] != nullptr) {
      in[i]->setConsumer(this);

      string desc =
        csprintf("[Queue from port %s %s %s to PerfectSwitch]",
                 to_string(m_switch_id), to_string(port),
                 to_string(i));

      in[i]->setDescription(desc);
      in[i]->setIncomingLink(port);
      in[i]->setVnet(i);
    }
  }
}

void
PerfectSwitch::addOutPort(const vector<MessageBuffer*>& out,
                          const NetDest& routing_table_entry)
{
  // Setup link order
  LinkOrder l;
  l.m_value = 0;
  l.m_link = m_out.size();
  m_link_order.push_back(l);

  // Add to routing table
  m_out.push_back(out);
  m_routing_table.push_back(routing_table_entry);
}

PerfectSwitch::~PerfectSwitch()
{
}

void
PerfectSwitch::operateVnet(int vnet)
{
  MsgPtr msg_ptr;
  NetworkMessage* net_msg_ptr = NULL;

  // This is for round-robin scheduling
  int incoming = m_round_robin_start;
  DPRINTF(RubyNetwork, "vnet %s \n", vnet);
  DPRINTF(RubyNetwork, "m_round_robin_start: %s \n", m_round_robin_start);
  DPRINTF(RubyNetwork, "m_in.size(): %s \n", m_in.size());
	
  m_round_robin_start++;
  if (m_round_robin_start >= m_in.size()) {
    m_round_robin_start = 0;
  }
	
  DPRINTF(RubyNetwork, "m_pending_message_count[vnet]: %s %s \n", vnet, m_pending_message_count[vnet]);
  if(m_pending_message_count[vnet] > 0) {
    // for all input ports, use round robin scheduling
    for (int counter = 0; counter < m_in.size(); counter++) {
      DPRINTF(RubyNetwork, "Counter: %d, incoming: %d, m_in.size(): %d\n", counter, incoming, m_in.size());
      // Round robin scheduling
      incoming++;
      if (incoming >= m_in.size()) {
        incoming = 0;
      }

      // temporary vectors to store the routing results
      vector<LinkID> output_links;
      vector<NetDest> output_link_destinations;

      DPRINTF(RubyNetwork, "Size of m_in[incoming] %d %d %d", incoming, vnet, m_in[incoming].size());

      // Is there a message waiting?
      if (m_in[incoming].size() <= vnet) {
        continue;
      }
						
      MessageBuffer *buffer = m_in[incoming][vnet];
      if (buffer == nullptr) {
        continue;
      }

#if (PRED_ARB == 0 )
      bool isReady = false;
      isReady = buffer->isReady();
      int isBusWon = false;
      DPRINTF(RubyNetwork, "isReady %d", isReady);

      while (isReady) {
#else
      while(buffer->isReady()){
#endif
        DPRINTF(RubyNetwork, "incoming: %d\n", incoming);

        // Peek at message
        msg_ptr = buffer->peekMsgPtr();
        net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
        DPRINTF(RubyNetwork, "Message: %s\n", (*net_msg_ptr));

        output_links.clear();
        output_link_destinations.clear();
        NetDest msg_dsts = net_msg_ptr->getInternalDestination();

        // Unfortunately, the token-protocol sends some
        // zero-destination messages, so this assert isn't valid
        // assert(msg_dsts.count() > 0);
        //

        DPRINTF(RubyNetwork, "msg_dsts %s \n", msg_dsts);
        DPRINTF(RubyNetwork, "m_link_order.size(): %s\n", m_link_order.size());
        assert(m_link_order.size() == m_routing_table.size());
        assert(m_link_order.size() == m_out.size());

        if (m_network_ptr->getAdaptiveRouting()) {
          if (m_network_ptr->isVNetOrdered(vnet)) {
            // Don't adaptively route
            DPRINTF(RubyNetwork, "This network is ordered\n");
            for (int out = 0; out < m_out.size(); out++) {
              m_link_order[out].m_link = out;
              m_link_order[out].m_value = 0;
            }
          } else {
            // Find how clogged each link is
            for (int out = 0; out < m_out.size(); out++) {
              int out_queue_length = 0;
              for (int v = 0; v < m_virtual_networks; v++) {
                out_queue_length += m_out[out][v]->getSize();
              }
              int value =
                (out_queue_length << 8) |
                random_mt.random(0, 0xff);
              m_link_order[out].m_link = out;
              m_link_order[out].m_value = value;
            }

            // Look at the most empty link first
            sort(m_link_order.begin(), m_link_order.end());
          }
        }
								
        for (int i = 0; i < m_routing_table.size(); i++) {
          // pick the next link to look at
          int link = m_link_order[i].m_link;
          NetDest dst = m_routing_table[link];

          if (!msg_dsts.intersectionIsNotEmpty(dst))
            continue;

          // Remember what link we're using
          DPRINTF(RubyNetwork, "link: %s dst: %s msg_dsts: %s\n", link, dst, msg_dsts);

          output_links.push_back(link);

          // Need to remember which destinations need this message in
          // another vector.  This Set is the intersection of the
          // routing_table entry and the current destination set.  The
          // intersection must not be empty, since we are inside "if"
                    	
          output_link_destinations.push_back(msg_dsts.AND(dst));


          // Next, we update the msg_destination not to include
          // those nodes that were already handled by this link
										
          // ANI HACK
          //msg_dsts.removeNetDest(dst);
        }



        // ANI HACK
        //assert(msg_dsts.count() == 0);

        // Check for resources - for all outgoing queues
        bool enough = true;
        for (int i = 0; i < output_links.size(); i++) {
          int outgoing = output_links[i];

          if (!m_out[outgoing][vnet]->areNSlotsAvailable(1))
            enough = false;

          DPRINTF(RubyNetwork, "Checking if node is blocked ..."
                  "outgoing: %d, vnet: %d, enough: %d\n",
                  outgoing, vnet, enough);
        }

        // There were not enough resources
        if (!enough) {
          scheduleEvent(Cycles(1));
          DPRINTF(RubyNetwork, "Can't deliver message since a node "
                  "is blocked\n");
          DPRINTF(RubyNetwork, "Message: %s\n", (*net_msg_ptr));
          break; // go to next incoming port
        }

        MsgPtr unmodified_msg_ptr;

        if (output_links.size() > 1) {
          // If we are sending this message down more than one link
          // (size>1), we need to make a copy of the message so each
          // branch can have a different internal destination we need
          // to create an unmodified MsgPtr because the MessageBuffer
          // enqueue func will modify the message

          // This magic line creates a private copy of the message
          unmodified_msg_ptr = msg_ptr->clone();
        }

        // Dequeue msg
        buffer->dequeue();
        m_pending_message_count[vnet]--;
								
								
        // ANI HACK
        // The idea is to not allow self router operations.
        // So, for this check if incoming == outgoing for all router nodes excluding crossbar
        // For cross bar, iterate over all the nodes and send the msg_ptr
        DPRINTF(RubyNetwork, "M switch id: %d\n", m_switch_id);
        for (int i=0; i<output_links.size(); i++) {
          int outgoing = output_links[i];

          if (i > 0) {
            // create a private copy of the unmodified message
            msg_ptr = unmodified_msg_ptr->clone();
          }

          // Change the internal destination set of the message so it
          // knows which destinations this link is responsible for.
          net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
          net_msg_ptr->getInternalDestination() =
            output_link_destinations[i];

          DPRINTF(RubyNetwork, "Last enqueued time: %d, delayed ticks: %d, getTime: %d\n", msg_ptr->getLastEnqueueTime(), msg_ptr->getDelayedTicks(), msg_ptr->getTime());

          // Enqeue msg
          // The value in the comparison should reflect the number of cores + memory
          // 6 means, 4 cores with 1 shared L2 and memory
          //
          if (m_switch_id < getNumberOfCores()+1) {
            if (incoming != outgoing) {
              DPRINTF(RubyNetwork, "Enqueuing net msg from "
                      "inport[%d][%d] to outport [%d][%d].\n",
                      incoming, vnet, outgoing, vnet);
										
              m_out[outgoing][vnet]->enqueue(msg_ptr);
              // Ani hack
              //m_out[outgoing][vnet]->enqueue(msg_ptr, Cycles(0));
            }
          }
          else {
            DPRINTF(RubyNetwork, "Enqueuing net msg from "
                    "inport[%d][%d] to outport [%d][%d].\n",
                    incoming, vnet, outgoing, vnet);
										
            // Ani hack
            m_out[outgoing][vnet]->enqueue(msg_ptr);									
            //m_out[outgoing][vnet]->enqueue(msg_ptr, Cycles(0));
          }	
        }
#if (PRED_ARB == 1)
      }
#else
      //for normal MSI bus, bus is owned in fifo
      //once the bus is used for txn, check if there is any ready message. If there is schedule wakeup.
      //This logic only appies to bus (switch_id == 9); TODO: Parametrize the id number  
  
      isReady = (m_switch_id == 9)? false : buffer->isReady();
      isBusWon = true;
      }

      if(isBusWon && (m_switch_id == 9) ){
       for(int counter = 0; counter < m_in.size(); counter++){
         MessageBuffer *buffer = m_in[counter][vnet];
         if (buffer == nullptr) {
           continue;
         }
         if(buffer->isReady()) scheduleEvent(Cycles(1));
       }
       return;
      }
#endif
//	
//        
//        // Enqueue it - for all outgoing queues
//        for (int i=0; i<output_links.size(); i++) {
//        int outgoing = output_links[i];
//
//        if (i > 0) {
//        // create a private copy of the unmodified message
//        msg_ptr = unmodified_msg_ptr->clone();
//        }
//
//        // Change the internal destination set of the message so it
//        // knows which destinations this link is responsible for.
//        net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
//        net_msg_ptr->getInternalDestination() =
//        output_link_destinations[i];
//
//        DPRINTF(RubyNetwork, "Last enqueued time: %d, delayed ticks: %d, getTime: %d\n", msg_ptr->getLastEnqueueTime(), msg_ptr->getDelayedTicks(), msg_ptr->getTime());
//
//        // Enqeue msg
//        DPRINTF(RubyNetwork, "Enqueuing net msg from "
//        "inport[%d][%d] to outport [%d][%d].\n",
//        incoming, vnet, outgoing, vnet);
//
//										
//        m_out[outgoing][vnet]->enqueue(msg_ptr);
//        }
//    
//								
    }
  }
}


void
PerfectSwitch::operateVnet(int incoming , int vnet, int pos)
{
  MsgPtr msg_ptr;
  NetworkMessage* net_msg_ptr = NULL;

  // This is for round-robin scheduling
  DPRINTF(RubyNetwork, "incoming %s vnet %s pos %s\n", incoming, vnet, pos);
  DPRINTF(RubyNetwork, "m_in.size(): %s \n", m_in.size());

  m_round_robin_start++;
  if (m_round_robin_start >= m_in.size()) {
    m_round_robin_start = 0;
  }

  DPRINTF(RubyNetwork, "m_pending_message_count[vnet]: %s %s \n",
          vnet, m_pending_message_count[vnet]);

  DPRINTF(RubyNetwork, "m_pending_message_count_mtx[incoming][vnet]: %S %s %s \n",
          incoming, vnet, m_pending_message_count_mtx[incoming][vnet]);

  if(m_pending_message_count[vnet] > 0) {
    DPRINTF(RubyNetwork, "incoming: %d, m_in.size(): %d\n",
            incoming, m_in.size());

    // temporary vectors to store the routing results
    vector<LinkID> output_links;
    vector<NetDest> output_link_destinations;

    DPRINTF(RubyNetwork, "Size of m_in[incoming] %d %d %d",
            incoming, vnet, m_in[incoming].size());

    // Is there a message waiting?
    if (m_in[incoming].size() <= vnet) {
      return;
    }
    
    MessageBuffer *buffer = m_in[incoming][vnet];
    if (buffer == nullptr) {
      return;
    }

    bool isReady = false;
    if(pos == -1)
      isReady = buffer->isReady();
    else
      isReady =  buffer->isReady(pos);

    DPRINTF(RubyNetwork, "isReady %d", isReady);

    while (isReady) {
      DPRINTF(RubyNetwork, "incoming: %d\n", incoming);

      // Peek at message
      if(pos == -1){
        msg_ptr = buffer->peekMsgPtr();
      }else{
        msg_ptr = buffer->peekMsgPtr(pos);
      }

      net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
      DPRINTF(RubyNetwork, "Message: %s\n", (*net_msg_ptr));

      output_links.clear();
      output_link_destinations.clear();
      NetDest msg_dsts = net_msg_ptr->getInternalDestination();

      // Unfortunately, the token-protocol sends some
      // zero-destination messages, so this assert isn't valid
      // assert(msg_dsts.count() > 0);
      //

      DPRINTF(RubyNetwork, "msg_dsts %s \n", msg_dsts);
      DPRINTF(RubyNetwork, "m_link_order.size(): %s\n", m_link_order.size());
      assert(m_link_order.size() == m_routing_table.size());
      assert(m_link_order.size() == m_out.size());

      for (int i = 0; i < m_routing_table.size(); i++) {
        // pick the next link to look at
        int link = m_link_order[i].m_link;
        NetDest dst = m_routing_table[link];

        if (!msg_dsts.intersectionIsNotEmpty(dst))
          continue;

        // Remember what link we're using
        DPRINTF(RubyNetwork, "link: %s dst: %s msg_dsts: %s\n", link, dst, msg_dsts);

        output_links.push_back(link);

        // Need to remember which destinations need this message in
        // another vector.  This Set is the intersection of the
        // routing_table entry and the current destination set.  The
        // intersection must not be empty, since we are inside "if"
        output_link_destinations.push_back(msg_dsts.AND(dst));


        // Next, we update the msg_destination not to include
        // those nodes that were already handled by this link
        // ANI HACK
        //msg_dsts.removeNetDest(dst);
      }

      // ANI HACK
      //assert(msg_dsts.count() == 0);

      // Check for resources - for all outgoing queues
      bool enough = true;
      for (int i = 0; i < output_links.size(); i++) {
        int outgoing = output_links[i];

        if (!m_out[outgoing][vnet]->areNSlotsAvailable(1))
          enough = false;

        DPRINTF(RubyNetwork, "Checking if node is blocked ..."
                "outgoing: %d, vnet: %d, enough: %d\n",
                outgoing, vnet, enough);
      }

      // There were not enough resources
      if (!enough) {
        scheduleEvent(Cycles(1));
        DPRINTF(RubyNetwork, "Can't deliver message since a node "
                "is blocked\n");
        DPRINTF(RubyNetwork, "Message: %s\n", (*net_msg_ptr));
        break; // go to next incoming port
      }

      MsgPtr unmodified_msg_ptr;

      if (output_links.size() > 1) {
        // If we are sending this message down more than one link
        // (size>1), we need to make a copy of the message so each
        // branch can have a different internal destination we need
        // to create an unmodified MsgPtr because the MessageBuffer
        // enqueue func will modify the message

        // This magic line creates a private copy of the message
        unmodified_msg_ptr = msg_ptr->clone();
      }

      // Dequeue msg
      if(pos == -1){
        buffer->dequeue();
      }else{
        buffer->dequeue(pos);
      }

      m_pending_message_count_mtx[incoming][vnet]--;

      m_pending_message_count[vnet]--;
			
      // ANI HACK
      // The idea is to not allow self router operations.
      // So, for this check if incoming == outgoing for all router
      // nodes excluding crossbar

      // For cross bar, iterate over all the nodes and send the msg_ptr
      DPRINTF(RubyNetwork, "M switch id: %d\n", m_switch_id);
      for (int i=0; i<output_links.size(); i++) {
        int outgoing = output_links[i];

        if (i > 0) {
          // create a private copy of the unmodified message
          msg_ptr = unmodified_msg_ptr->clone();
        }

        // Change the internal destination set of the message so it
        // knows which destinations this link is responsible for.
        net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
        net_msg_ptr->getInternalDestination() =
          output_link_destinations[i];

        DPRINTF(RubyNetwork, "Last enqueued time: %d, delayed ticks: %d, getTime: %d\n", msg_ptr->getLastEnqueueTime(), msg_ptr->getDelayedTicks(), msg_ptr->getTime());

        // Enqeue msg
        // The value in the comparison should reflect the number of cores + memory
        // 6 means, 4 cores with 1 shared L2 and memory
        //
        if (m_switch_id < getNumberOfCores()+1) {
          if (incoming != outgoing) {
            DPRINTF(RubyNetwork, "Enqueuing net msg from "
                    "inport[%d][%d] to outport [%d][%d].\n",
                    incoming, vnet, outgoing, vnet);
            
            m_out[outgoing][vnet]->enqueue(msg_ptr);
            // Ani hack
            //m_out[outgoing][vnet]->enqueue(msg_ptr, Cycles(0));
          }
        }
        else {
          DPRINTF(RubyNetwork, "Enqueuing net msg from "
                  "inport[%d][%d] to outport [%d][%d].\n",
                  incoming, vnet, outgoing, vnet);
          
          // Ani hack
          m_out[outgoing][vnet]->enqueue(msg_ptr);
          //m_out[outgoing][vnet]->enqueue(msg_ptr, Cycles(0));
        }	
      }

      //if(pos == -1)
      // isReady = buffer->isReady();
        // else
        isReady =  false;//buffer->isReady(pos);
    }
  }
}


void
PerfectSwitch::wakeup()
{

  //for switches other than the bus
#if( PRED_ARB == 1 )
  if(m_switch_id != 9){
#else
  if (1) {
#endif
    // Give the highest numbered link priority most of the time
    m_wakeups_wo_switch++;
    int highest_prio_vnet = m_virtual_networks-1;
    int lowest_prio_vnet = 0;
    int decrementer = 1;

    // invert priorities to avoid starvation seen in the component network
    if (m_wakeups_wo_switch > PRIORITY_SWITCH_LIMIT) {
      m_wakeups_wo_switch = 0;
      highest_prio_vnet = 0;
      lowest_prio_vnet = m_virtual_networks-1;
      decrementer = -1;
    }

    // For all components incoming queues
    for (int vnet = highest_prio_vnet;
         (vnet * decrementer) >= (decrementer * lowest_prio_vnet);
         vnet -= decrementer) {
      operateVnet(vnet);
    }
    scheduleEvent(Cycles(1));
    return;
  }

  ///////////////////////////////////////////////////////////////////////////
  //for switch == 9, bus arbitration
  ///////////////////////////////////////////////////////////////////////////

  std::vector<int> temp = getEventqDump();
  std::stringstream result;
  std::sort(temp.begin(), temp.end());
  std::copy(temp.begin(), temp.end(),
            std::ostream_iterator<int>(result, ","));

  DPRINTF(RubyNetwork,"#wakeup --> %s\n", result.str());
  DPRINTF(RubyNetwork,"#wakeup --> getSlotOwnerNew(): %s isStartDemandSlotNew: %s\n",
          getSlotOwnerNew(), isStartDemandSlotNew());
  

  if(isStartDemandSlotNew() && isHRTCore(getSlotOwnerNew())){
    scheduleEvent(Cycles(SLOT_WIDTH));
  }

  if(isSRTCore(getSlotOwnerNew())){
    scheduleEvent(Cycles(1));
  }

  if(isHRTCore(getSlotOwnerNew())){
    DPRINTF(RubyNetwork, "===> HRT\n");

    if(isStartDemandSlotNew()){
      m_slot_owner = getHRTCoreOfIndex(getSlotOwnerNew());
      m_is_slack_slot = false;
      int pos = isTherePendingResponse(8, m_slot_owner);
      
      DPRINTF(RubyNetwork, "HRT startslot slot_owner %s pos %s DemReq %s WBReq %s\n",
              m_slot_owner, pos, isTherePendingDemandReq(m_slot_owner),
              isTherePendingWBReq(m_slot_owner));

      if(isTherePendingDemandReq(m_slot_owner)
         || isTherePendingWBReq(m_slot_owner)
         || pos != -1){
        //HRT owner has request
        for(int i: {0, 1}){
          std::ignore = i;
          //DPRINTF(RubyNetwork, "HRT i %s", i);
          if(getToken(m_slot_owner) && (isTherePendingDemandReq(m_slot_owner)
                            || pos != -1)){
            if(pos != -1){
              //HRT has a response from memory
              operateVnet(8, 4, pos);
            }else{
              Address addr = getPendingDemandReqAddr(m_slot_owner);
              removeAllPendingResponseFromMEMToNRT(addr);
              operateVnet(m_slot_owner, 2, -1);
            }
           break;
          }else if(getToken(m_slot_owner) && isTherePendingWBReq(m_slot_owner)){
            operateVnet(m_slot_owner, 6, -1);
            break;
          }
          if(i == 0)getToken(m_slot_owner);
					//updateToken(m_slot_owner);
        }
      }else{
        //slack slot
        DPRINTF(RubyNetwork, "#wakeup slackslot\n");
        m_total_slack++;
#ifdef SLACK_SLOT

        m_is_slack_slot = true;
        bool is_slack_used = false;
        int index = m_srt_idx;

#ifdef DSA_SSA
        //DSA_SSA

        bool pendingWBToHRT = false;
        for(int i : SRT_CORES){
          if(isTherePendingWBToHRT(i)){
            pendingWBToHRT = true;
          }
        }

        if(pendingWBToHRT){
          DPRINTF(RubyNetwork, "slackslot DSA_SSA\n");
          //check first WBs which HRT is asking for

          do{
            DPRINTF(RubyNetwork, "slackslot srt %s WBToHRT %s\n",
                    getSRTCoreOfIndex(m_srt_idx),
                    isTherePendingWBToHRT(getSRTCoreOfIndex(m_srt_idx)));

            if(isTherePendingWBToHRT(getSRTCoreOfIndex(m_srt_idx))){
              operateVnet(getSRTCoreOfIndex(m_srt_idx), 6, -1);
              is_slack_used = true;
              break;
            }
            increSRTIdxRoundRobin();
          }while(index != m_srt_idx);

          if(is_slack_used){
            m_slot_owner = getSRTCoreOfIndex(m_srt_idx);
            increSRTIdxRoundRobin();
            return;
          }
        }
#endif
        // Default SSA
        DPRINTF(RubyNetwork, "slackslot DEFAULT_SSA\n");
        int nrt_resp = isTherePendingResponse(8, getNRTCoreOfIndex(0));

        // first check for NRT
        if(isTherePendingDemandReq(getNRTCoreOfIndex(0))
           || nrt_resp != -1){

          //There is a Req from a NRT or response from memory to a NRT
          DPRINTF(RubyNetwork, "slackslot NRT %s pos %s DemReq %s\n",
                  getSRTCoreOfIndex(0),
                  pos, isTherePendingDemandReq(getNRTCoreOfIndex(0)),
                  isTherePendingWBReq(getNRTCoreOfIndex(0)));

          if(nrt_resp != -1){
            operateVnet(8, 4, nrt_resp);
            is_slack_used = true;
          }else if(isTherePendingDemandReq(getNRTCoreOfIndex(0))){
            operateVnet(getNRTCoreOfIndex(0), 2, -1);
            is_slack_used = true;
          }
          m_slot_owner = getNRTCoreOfIndex(0);

        }

        else{

          //normal SRT demand or WB
          do{
            int pos = isTherePendingResponse(8,
                                             getSRTCoreOfIndex(m_srt_idx));

            DPRINTF(RubyNetwork, "slackslot srt %s pos %s DemReq %s WBReq %s\n",
                    getSRTCoreOfIndex(m_srt_idx),
                    pos, isTherePendingDemandReq(getSRTCoreOfIndex(m_srt_idx)),
                    isTherePendingWBReq(getSRTCoreOfIndex(m_srt_idx)));


            if(isTherePendingDemandReq(getSRTCoreOfIndex(m_srt_idx))
               || isTherePendingWBReq(getSRTCoreOfIndex(m_srt_idx))
               || pos != -1){
              for(int i: {0, 1}){
                std::ignore = i;
                //DPRINTF(RubyNetwork, "SRT i %s", i);
                if(getToken(getSRTCoreOfIndex(m_srt_idx)) &&
                   (isTherePendingDemandReq(getSRTCoreOfIndex(m_srt_idx))
                    || pos != -1) ){
                  if(pos != -1){
										DPRINTF(RubyNetwork, "SENDING MEMORY RESPONSE TO %d\n", getSRTCoreOfIndex(m_srt_idx));
                    operateVnet(8, 4, pos);

									DPRINTF(RubyNetwork, "MANAGING RETRY MAP FOR SRT CORE: %d\n", getSRTCoreOfIndex(m_srt_idx));
									// Manage retry map here
									for(std::map<Address, std::vector<nodeRetryCountPairType> >::iterator it = retryMap.begin(); it != retryMap.end(); it++) {
										for (int j = 0; j<it->second.size(); j++) {
											if (it->second[j].first == getSRTCoreOfIndex(m_srt_idx)) {
												DPRINTF(RubyNetwork, "FOUND SRT CORE ERASING IT FROM RETRY MAP...\n");
												it->second.erase(it->second.begin()+j);
											}
										}
										if (it->second.size() == 0) {
											retryMap.erase(it);
											break;
										}
									}
                  }else{
                    Address addr = getPendingDemandReqAddr(getSRTCoreOfIndex(m_srt_idx));
                    removeAllPendingResponseFromMEMToNRT(addr);
                    operateVnet(getSRTCoreOfIndex(m_srt_idx), 2, -1);
                  }
                  is_slack_used = true;
                  break;
                }else if(getToken(getSRTCoreOfIndex(m_srt_idx)) &&
                         isTherePendingWBReq(getSRTCoreOfIndex(m_srt_idx))){
                  operateVnet(getSRTCoreOfIndex(m_srt_idx), 6, -1);
                  is_slack_used = true;
                  break;
                }
                if(i == 0)getToken(getSRTCoreOfIndex(m_srt_idx));
								//updateToken(getSRTCoreOfIndex(m_srt_idx));
              }
              if(is_slack_used)
                break;
            }
            increSRTIdxRoundRobin();
          } while(index != m_srt_idx);

          if(is_slack_used){
            m_slot_owner = getSRTCoreOfIndex(m_srt_idx);
            increSRTIdxRoundRobin();
          }else{
            m_unused_slack++;
            DPRINTF(RubyNetwork, "#wakeup slackslot unused\n");
          }
        }
#endif
      }
    }else{
      //HRT non starting slot
      DPRINTF(RubyNetwork, "HRT non starting slot owner %s\n", m_slot_owner);

			// Why?
			// If owner is in M and sees a remote load, wb will not happen at start of slot
      if(isTherePendingResponse(m_slot_owner, {"DATA_TO_WB"})){
				DPRINTF(RubyNetwork, "SLOT OWNER IS DOING A WB\n");
        operateVnet(m_slot_owner, 4, -1);
      }
      else if(isTherePendingDemandReqSpecial(m_slot_owner)){
				DPRINTF(RubyNetwork, "SLOT OWNER IS PROCESSING A SPECIAL REQ\n");
        operateVnet(m_slot_owner, 2, -1);
      }
			else{

        int pos = isTherePendingResponse(8, m_slot_owner);

				DPRINTF(RubyNetwork, "CHECKING PENDING NO DATA ACKS TO %d\n", m_slot_owner);

				for (int srtcores: SRT_CORES) {
					Address Addr;
					int Pos = -1;
					if (isThereNoDataAck(srtcores, Addr, Pos)) {
						DPRINTF(RubyNetwork, "FOUND DATA ACK FOR SRT CORE: %d\n", srtcores);
						if (addressCoreRetryMap.find(Addr) == addressCoreRetryMap.end()) {
							std::vector<int> cores;
							cores.push_back(srtcores);
							addressCoreRetryMap.insert(std::pair<Address, std::vector<int> >(Addr, cores));
						}
						else {
							addressCoreRetryMap[Addr].push_back(srtcores);
						}
						operateVnet(srtcores, 6, Pos);
        		removeAllPendingResponseFromMEMToSRT(Addr, srtcores);
					}
				}

				if(pos != -1){
					DPRINTF(RubyNetwork, "SLOT OWNER IS GETTING RESPONSE FROM MEMORY\n");
          DPRINTF(RubyNetwork, "HRT response slot owner %s\n", m_slot_owner);
					// Why is this hard coded
          operateVnet(8, 4, pos);
        }else if(isTherePendingResponse(8)){
					DPRINTF(RubyNetwork, "SLACK SLOT OWNER IS GETTING RESPONSE FROM MEMORY\n");
          DPRINTF(RubyNetwork, "HRT response non slot owner %s\n", m_slot_owner);
          scheduleEvent(Cycles(1));
        }else{
					DPRINTF(RubyNetwork, "CHECKING IF SLOT OWNER IS RECIEVING VIA C2C\n");
          int sender_core, dest_core;
          int pos = isThereCacheToCachePendingResponseTo(m_slot_owner, sender_core);
          if(pos != -1){
						DPRINTF(RubyNetwork, "SENDER CORE FOUND: %d\n", sender_core);
            DPRINTF(RubyNetwork, "HRT response cache to cache %s\n", m_slot_owner);
            operateVnet(sender_core, 4, pos);
						DPRINTF(RubyNetwork, "SENDER CORE FOUND: %d\n", sender_core);
            int Data2WBPos = isTherePendingResponseToMemory(sender_core);
						DPRINTF(RubyNetwork, "CHECKING WB FOR CACHE TO CACHE TXN %d\n", Data2WBPos);
            if(Data2WBPos != -1){
							DPRINTF(RubyNetwork, "DOING WB FOR CACHE TO CACHE TXN \n");
              operateVnet(sender_core, 4, Data2WBPos);
            }
          }

          DPRINTF(RubyNetwork, "CHECKING IF SLOT OWNER IS SENDING VIA C2C\n");
          pos = -1;
          pos = isThereCacheToCachePendingResponseFrom(m_slot_owner, dest_core);
          if(pos != -1){
            DPRINTF(RubyNetwork, "DEST CORE FOUND: %d\n", dest_core);
            DPRINTF(RubyNetwork, "HRT response cache to cache Sender %s\n", m_slot_owner);
            operateVnet(m_slot_owner, 4, pos);
            DPRINTF(RubyNetwork, "SENDER CORE FOUND: %d\n", m_slot_owner);
            int Data2WBPos = isTherePendingResponseToMemory(m_slot_owner);
            DPRINTF(RubyNetwork, "CHECKING WB FOR CACHE TO CACHE TXN %d\n", Data2WBPos);
            if(Data2WBPos != -1){
              DPRINTF(RubyNetwork, "DOING WB FOR CACHE TO CACHE TXN \n");
              operateVnet(m_slot_owner, 4, Data2WBPos);
            }
          }
        }
      }
    }

  }else{
    //SRT slot frame
    DPRINTF(RubyNetwork, "===> SRT\n");

    if((isCurrentSRTReserveSlot() && isStartDemandSlotNew())){
      DPRINTF(RubyNetwork, "===> SRT Reserve Slot\n");
    }

    if(!isCurrentSRTReserveSlot() ||
       (isCurrentSRTReserveSlot() && isStartDemandSlotNew())){
      //non-reserve slot or reserve slot and SRT starting slot

      int index = m_srt_idx;

      DPRINTF(RubyNetwork, "SRT cutCycle - last_time_used :%s \n",
              g_system_ptr->curCycle() - m_last_time_srt_slot_used);
      if(g_system_ptr->curCycle() - m_last_time_srt_slot_used >= SLOT_WIDTH){
        bool isUsed = false;
        do{
          int pos = isTherePendingResponse(8, getSRTCoreOfIndex(m_srt_idx));

          DPRINTF(RubyNetwork, "SRT m_srt_idx:%s m_slot_owner:%s pos:%s\n",
                  getSRTCoreOfIndex(m_srt_idx), getSRTCoreOfIndex(m_srt_idx), pos );
          DPRINTF(RubyNetwork, "SRT DemReq:%s WBReq:%s \n",
                  isTherePendingDemandReq(getSRTCoreOfIndex(m_srt_idx)),
                  isTherePendingWBReq(getSRTCoreOfIndex(m_srt_idx)));


          if(isTherePendingDemandReq(getSRTCoreOfIndex(m_srt_idx))
             || isTherePendingWBReq(getSRTCoreOfIndex(m_srt_idx))
             || pos != -1){

            for(int i: {0, 1}){
              std::ignore = i;
              //DPRINTF(RubyNetwork, "SRT i %s", i);
              if( getToken(getSRTCoreOfIndex(m_srt_idx)) &&
                  (isTherePendingDemandReq(getSRTCoreOfIndex(m_srt_idx))
                   || pos != -1) ){
                if(pos != -1){

									DPRINTF(RubyNetwork, "RECEIVING SOMETHING FROM MEMORY SRT: %d\n", getSRTCoreOfIndex(m_srt_idx));
                  operateVnet(8, 4, pos);

									DPRINTF(RubyNetwork, "MANAGING RETRY MAP FOR SRT CORE: %d\n", getSRTCoreOfIndex(m_srt_idx));
									// Manage retry map here
									for(std::map<Address, std::vector<nodeRetryCountPairType> >::iterator it = retryMap.begin(); it != retryMap.end(); it++) {
										for (int j = 0; j<it->second.size(); j++) {
											if (it->second[j].first == getSRTCoreOfIndex(m_srt_idx)) {
												DPRINTF(RubyNetwork, "FOUND SRT CORE ERASING IT FROM RETRY MAP...\n");
												it->second.erase(it->second.begin()+j);
											}
										}
										if (it->second.size() == 0) {
											retryMap.erase(it);
											break;
										}
									}
                }else{
                  Address addr = getPendingDemandReqAddr(getSRTCoreOfIndex(m_srt_idx));
                  removeAllPendingResponseFromMEMToNRT(addr);
                  operateVnet(getSRTCoreOfIndex(m_srt_idx), 2, -1);
                }
                isUsed = true;
                break;
              }else if(getToken(getSRTCoreOfIndex(m_srt_idx)) &&
                       isTherePendingWBReq(getSRTCoreOfIndex(m_srt_idx))){
                operateVnet(getSRTCoreOfIndex(m_srt_idx), 6, -1);
                isUsed = true;
                break;
              }
              if(i == 0)getToken(getSRTCoreOfIndex(m_srt_idx));
							//updateToken(getSRTCoreOfIndex(m_srt_idx));
            }
            if(isUsed)
              break;
          }
          increSRTIdxRoundRobin();
        }while(index != m_srt_idx);

        if(isUsed){
          m_last_time_srt_slot_used = g_system_ptr->curCycle();
          m_slot_owner = getSRTCoreOfIndex(m_srt_idx);
          increSRTIdxRoundRobin();
        }

      }else{
        //SRT frame after SRT core sends Req on SRT frame
        if(isTherePendingResponse(m_slot_owner, {"DATA_TO_WB"})){
          operateVnet(m_slot_owner, 4, -1);
        }else if(isTherePendingDemandReqSpecial(m_slot_owner)){
          operateVnet(m_slot_owner, 2, -1);
        }else{
          int pos = isTherePendingResponse(8, m_slot_owner);
          DPRINTF(RubyNetwork, "SRT m_srt_idx:%s m_slot_owner:%s pos:%s\n",
                  getSRTCoreOfIndex(m_srt_idx), m_slot_owner, pos );
          if(pos != -1){
            operateVnet(8, 4, pos);
          }else{
            int sender_core, dest_core;
            int pos = isThereCacheToCachePendingResponseTo(m_slot_owner, sender_core);
            if(pos != -1){
              DPRINTF(RubyNetwork, "SRT response cache to cache %s\n", m_slot_owner);
              operateVnet(sender_core, 4, pos);
              int Data2WBPos = isTherePendingResponseToMemory(sender_core);
              if(Data2WBPos != -1){
                operateVnet(sender_core, 4, Data2WBPos);
              }
            }

            DPRINTF(RubyNetwork, "CHECKING IF SLOT OWNER IS SENDING VIA C2C\n");
            pos = -1;
            pos = isThereCacheToCachePendingResponseFrom(m_slot_owner, dest_core);
            if(pos != -1){
              DPRINTF(RubyNetwork, "DEST CORE FOUND: %d\n", dest_core);
              DPRINTF(RubyNetwork, "HRT response cache to cache Sender %s\n", m_slot_owner);
              operateVnet(m_slot_owner, 4, pos);
              DPRINTF(RubyNetwork, "SENDER CORE FOUND: %d\n", m_slot_owner);
              int Data2WBPos = isTherePendingResponseToMemory(m_slot_owner);
              DPRINTF(RubyNetwork, "CHECKING WB FOR CACHE TO CACHE TXN %d\n", Data2WBPos);
              if(Data2WBPos != -1){
                DPRINTF(RubyNetwork, "DOING WB FOR CACHE TO CACHE TXN \n");
                operateVnet(m_slot_owner, 4, Data2WBPos);
              }
            }
          }
        }
      }
    }else{
      //reserve slot and non starting slot
      if(isTherePendingResponse(m_slot_owner, {"DATA_TO_WB"})){
        operateVnet(m_slot_owner, 4, -1);
      }else if(isTherePendingDemandReqSpecial(m_slot_owner)){
        operateVnet(m_slot_owner, 2, -1);
      }else{
        int pos = isTherePendingResponse(8, m_slot_owner);
        DPRINTF(RubyNetwork, "SRT Reserve m_srt_idx:%s m_slot_owner:%s pos:%s\n",
                getSRTCoreOfIndex(m_srt_idx), m_slot_owner, pos );
        if(pos != -1){
          operateVnet(8, 4, pos);
        }else{
          int sender_core, dest_core;
          int pos = isThereCacheToCachePendingResponseTo(m_slot_owner, sender_core);
          if(pos != -1){
            DPRINTF(RubyNetwork, "SRT response cache to cache %s\n", m_slot_owner);
            operateVnet(sender_core, 4, pos);
            int Data2WBPos = isTherePendingResponseToMemory(sender_core);
            if(Data2WBPos != -1){
              operateVnet(sender_core, 4, Data2WBPos);
            }
          }

          DPRINTF(RubyNetwork, "CHECKING IF SLOT OWNER IS SENDING VIA C2C\n");
          pos = -1;
          pos = isThereCacheToCachePendingResponseFrom(m_slot_owner, dest_core);
          if(pos != -1){
            DPRINTF(RubyNetwork, "DEST CORE FOUND: %d\n", dest_core);
            DPRINTF(RubyNetwork, "HRT response cache to cache Sender %s\n", m_slot_owner);
            operateVnet(m_slot_owner, 4, pos);
            DPRINTF(RubyNetwork, "SENDER CORE FOUND: %d\n", m_slot_owner);
            int Data2WBPos = isTherePendingResponseToMemory(m_slot_owner);
            DPRINTF(RubyNetwork, "CHECKING WB FOR CACHE TO CACHE TXN %d\n", Data2WBPos);
            if(Data2WBPos != -1){
              DPRINTF(RubyNetwork, "DOING WB FOR CACHE TO CACHE TXN \n");
              operateVnet(m_slot_owner, 4, Data2WBPos);
            }
          }
        }
      }
    }
  }

  //ATOMIC requests
  operateVnet(5);
	// Cache to cache
	operateVnet(3);
}


bool
PerfectSwitch::isTherePendingDemandReq(int incoming){
  DPRINTF(RubyNetwork,"#isTherePendingDemandReq incoming %s\n", incoming);
  bool status = false;
	if (incoming == -1) return false;
  MessageBuffer* requestQueue = m_in[incoming][2];
  if(!requestQueue->isReady()){
    return false;
  }

  RequestMsg* reqmsg = safe_cast<RequestMsg*>(requestQueue->peekMsgPtr().get());

  DPRINTF(RubyNetwork,"#isTherePendingDemandReq reqmsg: %s %s\n",
          *reqmsg, CoherenceRequestType_to_string(reqmsg->m_Type));

  if(CoherenceRequestType_to_string(reqmsg->m_Type) == "GETM" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "GETMRR" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "GETS" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "GETMO" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "GETMA" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "UPG" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "GETI"){

    //I have pending request
    status = true;
  }
  //DPRINTF(RubyNetwork, "#isTherePendingRequestInRequestQueue :%s, %s\n",
  //        CoherenceRequestType_to_string(reqmsg->m_Type), status);
  DPRINTF(RubyNetwork,"#isTherePendingDemandReq status %s\n", status);
  return status;
}

bool
PerfectSwitch::isTherePendingDemandReqSpecial(int incoming){
  DPRINTF(RubyNetwork,"#isTherePendingDemandReqSpecial incoming %s\n", incoming);
  bool status = false;
  MessageBuffer* requestQueue = m_in[incoming][2];
  if(!requestQueue->isReady()){
    return false;
  }

  NetworkMessage* netmsg =
    safe_cast<NetworkMessage*>(requestQueue->peekMsgPtr().get());
  DPRINTF(RubyNetwork,"#isTherePendingDemandReqSpecial netmsg: %s %s\n", *netmsg,
          netmsg->getDestination().isSidePacket());

  if(netmsg->getDestination().isSidePacket()){
    //I have pending request
    status = true;
  }
  DPRINTF(RubyNetwork,"#isTherePendingDemandReqSpecial status %s\n", status);
  return status;
}

bool
PerfectSwitch::isTherePendingWBReq(int incoming){
  DPRINTF(RubyNetwork,"#isTherePendingWBReq incoming %s\n", incoming);

	if (incoming == -1) return false;
  bool status = false;
  MessageBuffer* WBQueue = m_in[incoming][6];
  if(!WBQueue->isReady()){
    return false;
  }

  RequestMsg* reqmsg = safe_cast<RequestMsg*>(WBQueue->peekMsgPtr().get());

  DPRINTF(RubyNetwork,"#isTherePendingWBReq reqmsg:%s %s\n",
          *reqmsg,
          CoherenceRequestType_to_string(reqmsg->m_Type));
  
  if(CoherenceRequestType_to_string(reqmsg->m_Type) == "PUTM"){
    //I have pending request
    status = true;
  }
  //DPRINTF(RubyNetwork, "#isTherePendingWBInRequestQueue :%s, %s\n",
  //        CoherenceRequestType_to_string(reqmsg->m_Type), status);
  DPRINTF(RubyNetwork,"#isTherePendingWBReq status %s\n", status);
  return status;
}

bool
PerfectSwitch::isTherePendingWBToHRT(int incoming){
  DPRINTF(RubyNetwork,"#isTherePendingWBToHRT incoming %s\n", incoming);

  bool status = false;
  MessageBuffer* WBQueue = m_in[incoming][6];
  if(!WBQueue->isReady()){
    return false;
  }

  RequestMsg* reqmsg = safe_cast<RequestMsg*>(WBQueue->peekMsgPtr().get());
	
  DPRINTF(RubyNetwork,"#isTherePendingWBToHRT reqmsg:%s %s %s\n",
          reqmsg, reqmsg->m_Dependent,
          CoherenceRequestType_to_string(reqmsg->m_Type));
  
  if(CoherenceRequestType_to_string(reqmsg->m_Type) == "PUTM"
     && isHRTCore(reqmsg->m_Dependent.getNum())){
    //I have pending request
    status = true;
  }
  //DPRINTF(RubyNetwork, "#isTherePendingWBInRequestQueue :%s, %s\n",
  //        CoherenceRequestType_to_string(reqmsg->m_Type), status);
  DPRINTF(RubyNetwork,"#isTherePendingWBToHRT status %s\n", status);
  return status;
}

bool
PerfectSwitch::isTherePendingResponse(int incoming, std::vector<string> types){
  DPRINTF(RubyNetwork,"#isTherePendingResponse incoming %s\n",
          incoming);
  bool status = false;
  MessageBuffer* responseQueue = m_in[incoming][4];
  if(!responseQueue->isReady()){
    return false;
  }
  ResponseMsg* resmsg =
    safe_cast<ResponseMsg*>(responseQueue->peekMsgPtr().get());
	
  DPRINTF(RubyNetwork,"#isTherePendingResponse resmsg:%s %s %s\n",
          resmsg, CoherenceResponseType_to_string(resmsg->m_Type));

  for(string str: types){
    if(CoherenceResponseType_to_string(resmsg->m_Type) == str){
      status = true;
    }
  }
  DPRINTF(RubyNetwork,"#isTherePendingResponse status %s\n", status);
  return status;
}


//check if there is pending response on vnet 4 destined to current owner
int
PerfectSwitch::isTherePendingResponse(int incoming, int destination)
{
  DPRINTF(RubyNetwork,"#isTherePendingResponse incoming %s destination %s\n",
          incoming, destination);
	
	if (destination == -1) {
		return -1;
	}

  MessageBuffer *responseQueue = m_in[incoming][4];
  MachineID ownerID = {MachineType_L1Cache, (unsigned int)destination};

  int pos = -1;
  for (int i = 0; i<responseQueue->getQSize(); i++) {
    if (responseQueue->isReady(i)) {
      MsgPtr msg_ptr = responseQueue->peekMsgPtr(i);
      NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
      DPRINTF(RubyNetwork,
              "ownerID: %s, destination: %s, isElement %s\n",
              ownerID, net_msg_ptr->getDestination(),
              net_msg_ptr->getDestination().isElement(ownerID));

      if (net_msg_ptr->getDestination().isElement(ownerID)) {
        DPRINTF(RubyNetwork, "index %i ...PENDING RESPONSE FOUND!\n", i);
        pos = i;
        break;
      }
      else {
        DPRINTF(RubyNetwork, "...PENDING RESPONSE NOT FOUND!\n");
      }
    }
  }
  DPRINTF(RubyNetwork,"#isTherePendingResponse pos %s\n", pos);
  return pos;
}

int
PerfectSwitch::isTherePendingResponseToMemory(int incoming)
{
  DPRINTF(RubyNetwork,"#isTherePendingResponseToMemory incoming %s\n",
          incoming);
	
	if (incoming == -1) {
		return -1;
	}

  MessageBuffer *responseQueue = m_in[incoming][4];
  MachineID ownerID = {MachineType_Directory, (NodeID)0};

  int pos = -1;
  for (int i = 0; i<responseQueue->getQSize(); i++) {
    if (responseQueue->isReady(i)) {
      MsgPtr msg_ptr = responseQueue->peekMsgPtr(i);
      NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
      DPRINTF(RubyNetwork,
              "ownerID: %s, destination: %s, isElement %s\n",
              ownerID, net_msg_ptr->getDestination(),
              net_msg_ptr->getDestination().isElement(ownerID));

			std::vector<NodeID> destVecTmp = net_msg_ptr->getDestination().getAllDest();

			for (unsigned int j = 0; j<destVecTmp.size(); j++) {
				DPRINTF(RubyNetwork, "DEST VEC TMP AT J: %d %s\n", j, destVecTmp.at(j));
			}

      if (net_msg_ptr->getDestination().isElement(ownerID)) {
        DPRINTF(RubyNetwork, "index %i ...PENDING RESPONSE FOUND!\n", i);
        pos = i;
        break;
      }
      else {
        DPRINTF(RubyNetwork, "...PENDING RESPONSE NOT FOUND!\n");
      }
    }
  }
  DPRINTF(RubyNetwork,"#isTherePendingResponse pos %s\n", pos);
  return pos;
}


bool
PerfectSwitch::isTherePendingResponse(int incoming)
{
  DPRINTF(RubyNetwork,"#isTherePendingResponse incoming %s\n",
          incoming);

	if (incoming == -1) return false;
  MessageBuffer *responseQueue = m_in[incoming][4];

  if (responseQueue->getQSize() && responseQueue->isReady()) {
    MsgPtr msg_ptr = responseQueue->peekMsgPtr();
    NetworkMessage* net_msg_ptr = safe_cast<NetworkMessage*>(msg_ptr.get());
    std::ignore = net_msg_ptr;
    DPRINTF(RubyNetwork, "#isTherePendingResponse destination: %s\n",
            net_msg_ptr->getDestination());
    DPRINTF(RubyNetwork, "...PENDING RESPONSE FOUND!\n");
    return true;
  }else {
    DPRINTF(RubyNetwork, "...PENDING RESPONSE NOT FOUND!\n");
    return false;
  }
}

int
PerfectSwitch::isThereCacheToCachePendingResponseTo(int incoming, int& sender_core)
{
  DPRINTF(RubyNetwork,"#isThereCacheToCachePendingResponseTo incoming %s\n",
          incoming);

	if (incoming == -1) return false;

  for(int core: HRT_CORES){
    if(core != incoming){
      int tmp = isTherePendingResponse(core, incoming);
      if (tmp != -1){
        sender_core = core;
        return tmp;
      }
    }
  }

  for(int core: SRT_CORES){
    if(core != incoming){
      int tmp = isTherePendingResponse(core, incoming);
      if (tmp != -1){
        sender_core = core;
        return tmp;
      }
    }
  }

	/*
	 * PAULOS
	for (int core: HRT_CORES) {
		if (core != incoming) {
			int tmp = isTherePendingResponse(incoming, core);
			if (tmp != -1) {
				sender_core = incoming;
				return tmp;
			}
		}
	}

	for (int core:SRT_CORES) {
		if (core != incoming) {
			int tmp = isTherePendingResponse(incoming, core);
			if (tmp != -1) {
				sender_core = incoming;
				return tmp;
			}
		}
	}
	*/

  sender_core = -1;
  return -1;
}

int
PerfectSwitch::isThereCacheToCachePendingResponseFrom(int sender_core, int & dst_core)
{
  DPRINTF(RubyNetwork,"#isThereCacheToCachePendingResponseTo sender_core %s\n",
          sender_core);

  if (sender_core == -1) return false;


   switch (sender_core) {
      case 0:
        for(int core: CORE_0_LINKS){
          if(core != sender_core){
            int tmp = isTherePendingResponse(sender_core, core);
            if (tmp != -1){
              dst_core = core;
              return tmp;
            }
          }
        }
      break;
      case 1:
        for(int core: CORE_1_LINKS){
          if(core != sender_core){
            int tmp = isTherePendingResponse(sender_core, core);
            if (tmp != -1){
              dst_core = core;
              return tmp;
            }
          }
        }
      break;
      case 2:
        for(int core: CORE_2_LINKS){
          if(core != sender_core){
            int tmp = isTherePendingResponse(sender_core, core);
            if (tmp != -1){
              dst_core = core;
              return tmp;
            }
          }
        }
      break;
      case 3:
        for(int core: CORE_3_LINKS){
          if(core != sender_core){
            int tmp = isTherePendingResponse(sender_core, core);
            if (tmp != -1){
              dst_core = core;
              return tmp;
            }
          }
        }
      break;
      case 4:
        for(int core: CORE_4_LINKS){
          if(core != sender_core){
            int tmp = isTherePendingResponse(sender_core, core);
            if (tmp != -1){
              dst_core = core;
              return tmp;
            }
          }
        }
      break;
      case 5:
        for(int core: CORE_5_LINKS){
          if(core != sender_core){
            int tmp = isTherePendingResponse(sender_core, core);
            if (tmp != -1){
              dst_core = core;
              return tmp;
            }
          }
        }
      break;
      case 6:
        for(int core: CORE_6_LINKS){
          if(core != sender_core){
            int tmp = isTherePendingResponse(sender_core, core);
            if (tmp != -1){
              dst_core = core;
              return tmp;
            }
          }
        }
      break;
      case 7:
        for(int core: CORE_7_LINKS){
          if(core != sender_core){
            int tmp = isTherePendingResponse(sender_core, core);
            if (tmp != -1){
              dst_core = core;
              return tmp;
            }
          }
        }
      break;
      default: ;
      }

  dst_core = -1;
  return -1;
}

bool
PerfectSwitch::isThereNoDataAck(int incoming, Address& addr, int& pos) {
	MessageBuffer* requestQueue = m_in[incoming][6];

	DPRINTF(RubyNetwork, "CHECKING FOR NO DATA MESSAGE FROM INCOMING: %d\n", incoming);
	if (!requestQueue->isReady()) {
		return false;
	}

	for (int i = 0; i<requestQueue->getQSize(); i++) { 
  	RequestMsg* reqmsg = safe_cast<RequestMsg*>(requestQueue->peekMsgPtr(i).get());

		if (CoherenceRequestType_to_string(reqmsg->m_Type) == "NODATA") {

		  DPRINTF(RubyNetwork," FOUND NO DATA ACK reqmsg: %s %s\n",
          *reqmsg, CoherenceRequestType_to_string(reqmsg->m_Type));
			addr = reqmsg->getAddr();
			pos = i;
			return true;
		}
	}

	return false;
}

Address
PerfectSwitch::getPendingDemandReqAddr(int incoming){
  DPRINTF(RubyNetwork,"#getPendingDemandReqAddr incoming %s\n", incoming);
  Address addr = Address();
	if (incoming == -1) return addr;
  MessageBuffer* requestQueue = m_in[incoming][2];
  if(!requestQueue->isReady()){
    return addr;
  }

  RequestMsg* reqmsg = safe_cast<RequestMsg*>(requestQueue->peekMsgPtr().get());

  DPRINTF(RubyNetwork,"#getPendingDemandReqAddr reqmsg: %s %s\n",
          *reqmsg, CoherenceRequestType_to_string(reqmsg->m_Type));

  if(CoherenceRequestType_to_string(reqmsg->m_Type) == "GETM" ||
     // CoherenceRequestType_to_string(reqmsg->m_Type) == "GETS" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "GETMRR" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "GETMO" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "GETMA" ||
     CoherenceRequestType_to_string(reqmsg->m_Type) == "UPG"){

    //I have pending request
    addr = reqmsg->getAddr();
  }
  //DPRINTF(RubyNetwork, "#isTherePendingRequestInRequestQueue :%s, %s\n",
  //        CoherenceRequestType_to_string(reqmsg->m_Type), status);
  DPRINTF(RubyNetwork,"#getPendingDemandReqAddr addr %s\n", addr);
  return addr;
}

bool
PerfectSwitch::removeAllPendingResponseFromMEMToNRT(Address addr)
{

  DPRINTF(RubyNetwork,"#removeAllPendingResponseFromMEMToNRT addr %s\n", addr);

  bool rtr = false;

#if C2C_TYPE == 1
  DPRINTF(RubyNetwork,"#removeAllPendingResponseFromMEMToNRT Trace based enabled %s\n", addr);
  if (addr == Address(0)) {
		return rtr;
	}

  MessageBuffer *responseQueue = m_in[getNumberOfCores()][4];

  for (int i = 0; i<responseQueue->getQSize(); i++) {
    if (responseQueue->isReady(i)) {
      ResponseMsg* res_msg_ptr = safe_cast<ResponseMsg*>( responseQueue->peekMsgPtr(i).get());
      
      if (addr == res_msg_ptr->getAddr()) {
        DPRINTF(RubyNetwork,"#removeAllPendingResponseFromMEMToNRT res_msg_ptr->getAddr() %s\n", res_msg_ptr->getAddr());

        std::vector<NodeID> destIDs = res_msg_ptr->getDestination().getAllDest();
        
        bool remove = false;
        for (int j = 0; j < destIDs.size() ; ++j) {
          if (isNRTCore(destIDs[j])){
            DPRINTF(RubyNetwork,"#removeAllPendingResponseFromMEMToNRT  isNRT(%s) = %s\n", destIDs[j], isNRTCore(destIDs[j]));
            remove = true;
            break;
          }
        }

        if(remove){
          responseQueue->removeMessage(i);
          rtr = true;
        }
      }
    }
  }
#endif
  return rtr;
}

void
PerfectSwitch::removeAllPendingResponseFromMEMToSRT(Address addr, int srtID) {
	
  DPRINTF(RubyNetwork,"#removeAllPendingResponseFromMEMToSRT addr %s\n", addr);

#if C2C_TYPE == 1
  DPRINTF(RubyNetwork,"#removeAllPendingResponseFromMEMToSRT Trace based enabled %s\n", addr);
  if (addr == Address(0)) {
		return;
	}

	MessageBuffer* responseQueue = m_in[getNumberOfCores()][4];

	DPRINTF(RubyNetwork, "RESPONSE QUEUE SIZE: %d\n", responseQueue->getQSize());
	for (int i = 0; i<responseQueue->getQSize(); i++) {
		if (responseQueue->isReady(i)) {
   		ResponseMsg* res_msg_ptr = safe_cast<ResponseMsg*>( responseQueue->peekMsgPtr(i).get());

    	DPRINTF(RubyNetwork,"MESSAGE #removeAllPendingResponseFromMEMToSRT res_msg_ptr->getAddr() %s\n", res_msg_ptr->getAddr()); 
     	if (addr == res_msg_ptr->getAddr()) {
     	  DPRINTF(RubyNetwork,"#removeAllPendingResponseFromMEMToSRT res_msg_ptr->getAddr() %s\n", res_msg_ptr->getAddr());

				// Does the response queue have one entry per address, or one entry per request/response
     	  std::vector<NodeID> destIDs = res_msg_ptr->getDestination().getAllDest();

				for (int j = 0; j<destIDs.size(); j++) {
					DPRINTF(RubyNetwork, "DEST IDS %d\n", destIDs[j]);
				}
	
 	      for (int j = 0; j < destIDs.size() ; ++j) {
					if (destIDs[j] == srtID) {
						responseQueue->removeMessage(i);

						for (int k = 0; k<addressCoreRetryMap[addr].size(); k++) {
							if (addressCoreRetryMap[addr][k] == srtID) {
								addressCoreRetryMap[addr].erase(addressCoreRetryMap[addr].begin()+k);
								break;
							}
						}
						if (addressCoreRetryMap[addr].size() == 0) {
							addressCoreRetryMap.erase(addr);
						}
						break;
					}
       	}
     	}
   	}
 	}

	MessageBuffer* requestQueue = m_in[getNumberOfCores()][2];
	DPRINTF(RubyNetwork, "REQUEST QUEUE SIZE: %d\n", requestQueue->getQSize());
	for (int i = 0; i<requestQueue->getQSize(); i++) {
   	RequestMsg* req_msg_ptr = safe_cast<RequestMsg*>( requestQueue->peekMsgPtr(i).get());

   	DPRINTF(RubyNetwork,"MESSAGE #removeAllPendingResponseFromMEMToSRT req_msg_ptr->getAddr() %s\n", req_msg_ptr->getAddr()); 
   	if (addr == req_msg_ptr->getAddr()) {
   	  DPRINTF(RubyNetwork,"#removeAllPendingResponseFromMEMToSRT req_msg_ptr->getAddr() %s\n", req_msg_ptr->getAddr());

			if (srtID == req_msg_ptr->getRequestor().getNum()) {
				requestQueue->removeMessage(i);

				for (int k = 0; k<addressCoreRetryMap[addr].size(); k++) {
					if (addressCoreRetryMap[addr][k] == srtID) {
						addressCoreRetryMap[addr].erase(addressCoreRetryMap[addr].begin()+k);
						break;
					}
				}
				if (addressCoreRetryMap[addr].size() == 0) {
					addressCoreRetryMap.erase(addr);
				}
				break;
			}
   	}	
 	}
#endif
}

void
PerfectSwitch::regStats(string parent)
{
  m_total_slack.name(parent + csprintf(".PerfectSwitch%d", m_switch_id) + ".slack_total_slots");
  m_unused_slack.name(parent + csprintf(".PerfectSwitch%d", m_switch_id) + ".slack_unused_slots");
}

void
PerfectSwitch::storeEventInfo(int info)
{
  m_pending_message_count[info]++;
}

void
PerfectSwitch::storeEventInfo(int link_id , int vnet)
{
  m_pending_message_count_mtx[link_id][vnet]++;
}

void
PerfectSwitch::clearStats()
{
}
void
PerfectSwitch::collateStats()
{
}


void
PerfectSwitch::print(std::ostream& out) const
{
  out << "[PerfectSwitch " << m_switch_id << "]";
}
