#!/bin/bash

if [ ! -d eembc-$1 ]; then
    mkdir eembc-$1
fi

for j in a2time01-trace aifirf01-trace basefp01-trace \
                       cacheb01-trace empty-trace iirflt01-trace \
                       pntrch01-trace rspeed01-trace ttsprk01-trace;
#for j in a2time01-trace
do
    if [ ! -d eembc-$1/$j ]; then
        mkdir eembc-$1/$j
    fi

    for  i in {0..7};
    do

        #if [ $i == 1 ] || [ $i == 5 ] || [ $i == 7 ]; then
        #    touch eembc-$1/$j/trace$i.trc
        #else
		    cp eembc-traces/$j/trace_C0.trc eembc-$1/$j/trace$i.trc
        #fi

        if [ $i = 7 ] ;then
            sed -i 's/WR/RD/g' eembc-$1/$j/trace$i.trc
        fi
    done

    ./build/X86_MCS_PMSI_CDR/gem5.opt \
				--debug-flags=ProtocolTrace \
        -d eembc-$1/$j/ \
        ./configs/example/ruby_random_test.py \
        --ruby-clock=2GHz \
        --ruby \
        --cpu-clock=2GHz \
        --topology=Crossbar \
        --mem-type=SimpleMemory \
        -n 8 \
        --mem-size=4194304kB \
        --maxloads=100000 \
        --wakeup_freq=1 \
        --trace_path eembc-$1/$j/ \
        &> eembc-$1/$j/debug &
done
